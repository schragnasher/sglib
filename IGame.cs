﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SimpleGamesLibrary.Utilities;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary {

    public interface IGame {
        Form Form { get; }
        GraphicsDevice GraphicsDevice { get; }
        GameServices GameServices { get; }
        Settings Settings { get; }
        ResourceManager<TextureResource, TextureResourceDescriptor> TextureResourceManager { get; }
        ResourceManager<FontResource, FontResourceDescriptor> FontResourceManager { get; }
        ResourceManager<SoundResource, SoundResourceDescriptor> SoundResourceManager { get; }   

        void Quit();
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Xna;

namespace SimpleGamesLibrary.PhysicsSystem {
    public class PhysicsManager : IPhysicsManagerService {
        private readonly IGame Game;

        public World World { get; private set; }

        private bool isDebugEnabled = true;
        //private DebugViewSGL _debugView; //TODO: When J creates custom SpriteBatch, can do this

        public PhysicsManager(IGame game) {
            Game = game;
            Game.GameServices.RegisterService<IPhysicsManagerService>(this);

            World = new World(Vector2.Zero);

            //TODO: When J creates custom SpriteBatch, can do this
            //if (isDebugEnabled)
            //    _debugView = new DebugViewSGL(Game, World);
        }

        public void Initialize() {

        }

        public void Update(float deltaTime) {
            World.Step(deltaTime);

            //TODO: When J creates custom SpriteBatch, can do this
            //if (isDebugEnabled)
            //    ;//  _debugView.
        }
    }
}

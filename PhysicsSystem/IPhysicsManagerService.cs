﻿using FarseerPhysics.Dynamics;
namespace SimpleGamesLibrary.PhysicsSystem {
    public interface IPhysicsManagerService {
        World World { get; }
    }
}

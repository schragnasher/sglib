﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.GUISystem {

    public class GUISkin {

        public TextureResource ButtonUpImage;
        public TextureResource ButtonDownImage;
        public TextureResource ButtonOverImage;
        public FontResource ButtonFont;

        public FontResource LabelFont;

        public TextureResource SliderButton;
        public TextureResource SliderTrack;

        public TextureResource CheckBoxEmpty;
        public TextureResource CheckBoxChecked;

        public GUISkin() { }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.GUISystem {
    public struct LabelDescriptor {
        public string id;
        public int xPosition;
        public int yPosition;
        public int width;
        public int height;
        public string text;

        public LabelDescriptor(string id, int xPosition, int yPosition, int width, int height, string text) {
            this.id = id;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.width = width;
            this.height = height;
            this.text = text;
        }
    }

    public struct ButtonDescriptor {
        public string id;
        public string label;
        public int xPosition;
        public int yPosition;
        public int width;
        public int height;

        public ButtonDescriptor(string id, int xPosition, int yPosition, int width, int height, string label) {
            this.id = id;
            this.label = label;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.width = width;
            this.height = height;
        }
    }

    public struct SliderDescriptor {
        public string id;
        public int xPosition;
        public int yPosition;
        public int width;
        public int height;
        public float currentValue;
        public float minValue;
        public float maxValue;

        public SliderDescriptor(string id, int xPosition, int yPosition, int width, int height, float sliderValue, float minValue = 0.0f, float maxValue = 100.0f) {
            this.id = id;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.width = width;
            this.height = height;
            this.currentValue = sliderValue;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }
    }
    public struct CheckboxDescriptor {
        public string id;
        public int xPosition;
        public int yPosition;
        public int width;
        public int height;
        public bool isChecked;

        public CheckboxDescriptor(string id, int xPosition, int yPosition, int width, int height, bool isChecked) {
            this.id = id;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.width = width;
            this.height = height;
            this.isChecked = isChecked;
        }
    }

    public struct ImageDescriptor {
        public string id;
        public int xPosition;
        public int yPosition;
        public int width;
        public int height;
        public TextureResource texture;

        public ImageDescriptor(string id, int xPosition, int yPosition, int width, int height, TextureResource texture) {
            this.id = id;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.width = width;
            this.height = height;
            this.texture = texture;
        }
    }
}

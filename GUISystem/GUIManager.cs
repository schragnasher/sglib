﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SlimDX;
using SlimDX.Direct3D9;
using SimpleGamesLibrary.InputSystem;
using SimpleGamesLibrary.Utilities;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.GUISystem {
    public class GUIManager : IGUIManagerService, IDisposable {

        public event Action GUIEvent;

        private readonly IGame _game;
        private IInputManagerService _inputManager;

        private string over = "";
        private string hot = "";
        private string active = "";

        public GUISkin Skin { get; set; }

        // Input Properties
        private Point mousePos = new Point(0, 0);
        private bool leftMouseButtonDown = false;
        private IMouse _mouse;
        
        public GUIManager(IGame game) {
            _game = game;
            _game.GameServices.RegisterService<IGUIManagerService>(this);
        }

        public void Initialize() {
            _inputManager = _game.GameServices.GetService<IInputManagerService>();

            _mouse = _inputManager.GetMouse();
            _mouse.MouseMoveEvent += MouseMoveHandler;
            _mouse.MouseButtonDownEvent += MouseButtonDownHandler;
            _mouse.MouseButtonUpEvent += MouseButtonUpHandler;
        }

        public void Render() {
            hot = over;
            over = "";
            _game.GraphicsDevice.Device.SetTransform(TransformState.View, ResetView());
            OnGUIevent();
            _game.GraphicsDevice.Device.SetTransform(TransformState.View, Matrix.Identity); //TODO: Decide if this is needed (J!)
        }

        // Camera needs to be reset so the origin for all GUI elements is the top-left corner
        // of the screen.
        private Matrix ResetView() {
            float centerOfScreenX = (float)-_game.GraphicsDevice.ResolutionX / 2;
            float centerOfScreenY = (float)_game.GraphicsDevice.ResolutionY / 2;

            return (Matrix.RotationX(3.14f) * Matrix.Translation(centerOfScreenX, centerOfScreenY, 0));
        }

        private void OnGUIevent() {
            if (GUIEvent != null) { GUIEvent(); }
        }

        #region Gui Element Functions
        public bool Button(ButtonDescriptor buttonDescriptor) {
            return Button(buttonDescriptor.id, 
                new Rectangle(buttonDescriptor.xPosition, 
                    buttonDescriptor.yPosition, buttonDescriptor.width, 
                    buttonDescriptor.height), buttonDescriptor.label);
        }
        public bool Button(string elementId, Rectangle rectangle, string text) {
            bool result = false;

            Texture tex = null;

            if (active.Equals(elementId)) {
                if (!leftMouseButtonDown) {
                    if (hot.Equals(elementId)) {
                        result = true;
                    }
                    active = "";
                }
            }
            else if (hot.Equals(elementId)) {
                if (leftMouseButtonDown) {
                    if (active == "") { active = elementId; }
                }
            } 
            
            if (rectangle.Contains(mousePos)) {
                if (active.Equals(elementId) || active == "") { over = elementId; }
            }

            if (active.Equals(elementId)) { tex = Skin.ButtonDownImage.Texture; }
            else if (hot.Equals(elementId)) { tex = Skin.ButtonOverImage.Texture; }
            else { tex = Skin.ButtonUpImage.Texture; }

            ColoredTexturedVertex[] vertices = {
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y,0), Color.White, 0.0f, 0.0f),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y,0), Color.White, 1.0f, 0.0f),
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y + rectangle.Height,0), Color.White, 0.0f, 1.0f),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height,0), Color.White, 1.0f, 1.0f)
            };

            _game.GraphicsDevice.Device.VertexFormat = ColoredTexturedVertex.Format;
            _game.GraphicsDevice.Device.SetTexture(0, tex);
            _game.GraphicsDevice.Device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);
            Skin.ButtonFont.Font.DrawString(null, text, rectangle, DrawTextFormat.Center | DrawTextFormat.VerticalCenter, Color.White);

            return result;
        }

        public void Label(LabelDescriptor labelDescriptor) {
            Label(labelDescriptor.id, new Rectangle(labelDescriptor.xPosition,
                labelDescriptor.yPosition, labelDescriptor.width,
                labelDescriptor.height), labelDescriptor.text);
        }

        public void Label(string elementId, Rectangle rectangle, string text) {
            Skin.LabelFont.Font.DrawString(null, text, rectangle, DrawTextFormat.Center | DrawTextFormat.VerticalCenter, Color.White);
        }

        public float Slider(SliderDescriptor sliderDescriptor) {
            return Slider(sliderDescriptor.id, new Rectangle(sliderDescriptor.xPosition,
                sliderDescriptor.yPosition, sliderDescriptor.width, sliderDescriptor.height),
                sliderDescriptor.currentValue, sliderDescriptor.minValue, sliderDescriptor.maxValue);
        }

        public float Slider(string elementId, Rectangle rectangle, float value, float minValue, float maxValue) {

            float sliderwidth = Math.Abs(minValue - maxValue);
            float sliderratio = ((value - minValue) / sliderwidth);
            int buttonx = (int)(rectangle.Width * sliderratio);

            Rectangle button = new Rectangle(
                rectangle.X + buttonx - (Skin.SliderButton.Info.Width / 2), 
                rectangle.Y + (rectangle.Height / 2) - (Skin.SliderButton.Info.Height / 2),
                Skin.SliderButton.Info.Width,
                Skin.SliderButton.Info.Height);

            Rectangle track = new Rectangle(
                rectangle.X, 
                rectangle.Y + (rectangle.Height / 2) - (Skin.SliderTrack.Info.Height / 2), 
                rectangle.Width, 
                Skin.SliderTrack.Info.Height);

            if (active.Equals(elementId)) {
                float mouseratio = (float)((mousePos.X - rectangle.X) / (float)(rectangle.Width));
                value = sliderwidth * mouseratio + minValue;
                if (!leftMouseButtonDown) {
                    active = "";
                }
            }
            else if (hot.Equals(elementId)) {
                if (leftMouseButtonDown) {
                    if (active == "") { active = elementId; }
                }
            }

            if (rectangle.Contains(mousePos)) {
                if (active.Equals(elementId) || active == "") { over = elementId; }
            }

            #region draw slider
            ColoredTexturedVertex[] vertices = {
                new ColoredTexturedVertex(new Vector3(track.X, track.Y,0), Color.White, 0, 0),
                new ColoredTexturedVertex(new Vector3(track.X + track.Width, track.Y,0), Color.White, 1, 0),
                new ColoredTexturedVertex(new Vector3(track.X, track.Y + track.Height,0), Color.White, 0, 1),
                new ColoredTexturedVertex(new Vector3(track.X + track.Width, track.Y + track.Height,0), Color.White, 1, 1)
            };
            _game.GraphicsDevice.Device.SetTexture(0, Skin.SliderTrack.Texture);
            _game.GraphicsDevice.Device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);

            ColoredTexturedVertex[] vertices2 = {
                new ColoredTexturedVertex(new Vector3(button.X, button.Y,0), Color.White, 0, 0),
                new ColoredTexturedVertex(new Vector3(button.X + button.Width, button.Y,0), Color.White, 1, 0),
                new ColoredTexturedVertex(new Vector3(button.X, button.Y + button.Height,0), Color.White, 0, 1),
                new ColoredTexturedVertex(new Vector3(button.X + button.Width, button.Y + button.Height,0), Color.White, 1, 1)
            };

            _game.GraphicsDevice.Device.VertexFormat = ColoredTexturedVertex.Format;
            _game.GraphicsDevice.Device.SetTexture(0, Skin.SliderButton.Texture);
            _game.GraphicsDevice.Device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices2);
            #endregion

            if (value > maxValue) { value = maxValue; }
            if (value < minValue) { value = minValue; }
            return value;
        }

        public bool CheckBox(CheckboxDescriptor checkboxDescriptor) {
            return CheckBox(checkboxDescriptor.id, new Rectangle(checkboxDescriptor.xPosition,
                checkboxDescriptor.yPosition, checkboxDescriptor.width, checkboxDescriptor.height),
                checkboxDescriptor.isChecked);
        }

        public bool CheckBox(string elementId, Rectangle rectangle, bool ischecked) {
            if (active.Equals(elementId)) {
                if (!leftMouseButtonDown) {
                    if (hot.Equals(elementId)) {
                        ischecked = !ischecked;
                    }
                    active = "";
                }
            }
            else if (hot.Equals(elementId)) {
                if (leftMouseButtonDown) {
                    if (active == "") { active = elementId; }
                }
            }

            if (rectangle.Contains(mousePos)) {
                if (active.Equals(elementId) || active == "") { over = elementId; }
            }

            #region draw checkbox
            ColoredTexturedVertex[] vertices = {
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y,0), Color.White, 0, 0),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y,0), Color.White, 1, 0),
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y + rectangle.Height,0), Color.White, 0, 1),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height,0), Color.White, 1, 1)
            };

            if (ischecked) {
                _game.GraphicsDevice.Device.SetTexture(0, Skin.CheckBoxChecked.Texture);
            }
            else {
                _game.GraphicsDevice.Device.SetTexture(0, Skin.CheckBoxEmpty.Texture);
            }

            _game.GraphicsDevice.Device.VertexFormat = ColoredTexturedVertex.Format;
            _game.GraphicsDevice.Device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);
            #endregion

            return ischecked;
        }

        public void Image(ImageDescriptor imageDescriptor) {
            Image(imageDescriptor.id, new Rectangle(imageDescriptor.xPosition, imageDescriptor.yPosition, imageDescriptor.width, imageDescriptor.height), imageDescriptor.texture);
        }

        public void Image(string elementId, Rectangle rectangle, TextureResource texture) {
            ColoredTexturedVertex[] vertices = {
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y,0), Color.White, 0, 0),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y,0), Color.White, 1, 0),
                new ColoredTexturedVertex(new Vector3(rectangle.X, rectangle.Y + rectangle.Height,0), Color.White, 0, 1),
                new ColoredTexturedVertex(new Vector3(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height,0), Color.White, 1, 1)
            };

            _game.GraphicsDevice.Device.VertexFormat = ColoredTexturedVertex.Format;
            _game.GraphicsDevice.Device.SetTexture(0, texture.Texture);
            _game.GraphicsDevice.Device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);
        }
        #endregion

        public void LoadSkin() {

        }

        public void MouseMoveHandler(object sender, MouseEventArgs e) {
            mousePos = e.Location;
        }

        public void MouseButtonDownHandler(object sender, MouseEventArgs e) {
            if (e.Button.Equals(MouseButtons.Left)) { leftMouseButtonDown = true; }
        }

        public void MouseButtonUpHandler(object sender, MouseEventArgs e) {
            if (e.Button.Equals(MouseButtons.Left)) { leftMouseButtonDown = false; }
        }

        public void Dispose() {
            _mouse.MouseMoveEvent -= MouseMoveHandler;
            _mouse.MouseButtonDownEvent -= MouseButtonDownHandler;
            _mouse.MouseButtonUpEvent -= MouseButtonUpHandler;
        }
    }
}

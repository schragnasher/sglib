﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.GUISystem {
    public interface IGUIManagerService {
        event Action GUIEvent;
        void Label(LabelDescriptor labelDescriptor);
        void Label(string elementId, Rectangle rectangle, string text);
        bool Button(ButtonDescriptor buttonDescriptor);
        bool Button(string elementId, Rectangle rectangle, string text);
        float Slider(SliderDescriptor sliderDescriptor);
        float Slider(string elementId, Rectangle rectangle, float value, float minValue, float maxValue);
        bool CheckBox(CheckboxDescriptor checkboxDescriptor);
        bool CheckBox(string elementId, Rectangle rectangle, bool ischecked);
        void Image(string elementId, Rectangle rectangle, TextureResource texture);
        GUISkin Skin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.AudioSystem {
    public interface IAudioService {
        float MasterVolume { get; set; }
        float SoundVolume { get; set; } 
        float MusicVolume { get; set; }
        void PlaySound(SoundResource soundResource, bool isLooped);
        void PlayMusic(SoundResource musicResource, bool isLooped);
        bool IsSoundPlaying(string filepath);
        void StopAllSounds();
    }
}

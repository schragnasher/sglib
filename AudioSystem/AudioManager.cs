﻿using IrrKlang;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.AudioSystem {
    public class AudioManager : IAudioService {

        private readonly IGame Game;
        private readonly ISoundEngine SoundEngine;
        private ISoundEffectControl FxControl;
        private ISound _currentMusic;

        private float _masterVolume = 100;
        private float _soundVolume = 100;
        private float _musicVolume = 100;

        public AudioManager(IGame game) {
            Game = game;
            Game.GameServices.RegisterService<IAudioService>(this);
            SoundEngine = new ISoundEngine();
            FxControl = null;
        }

        public void Initialize() {
            MasterVolume = Game.Settings.MasterVolume;
            SoundVolume = Game.Settings.SoundVolume;
            MusicVolume = Game.Settings.MusicVolume;
        }

        #region Volume
        public float MasterVolume {
            set {
                _masterVolume = value;
                SoundEngine.SoundVolume = _masterVolume / 100.0f;
                Game.Settings.MasterVolume = _masterVolume;
            }
            get {
                return _masterVolume;
            }
        }

        public float SoundVolume {
            set {
                _soundVolume = value;
                Game.Settings.SoundVolume = _soundVolume;
            }
            get {
                return _soundVolume;
            }
        }

        public float MusicVolume {
            set {
                _musicVolume = value;
                Game.Settings.MusicVolume = _musicVolume;
                if (_currentMusic != null)
                    _currentMusic.Volume = _musicVolume / 100.0f;
            }
            get {
                return _musicVolume;
            }
        }
        #endregion


        public void PlaySound(SoundResource soundResource, bool isLooped) {
            System.Diagnostics.Debug.Assert(soundResource != null, "Sound resource is null; cannot play it");
            System.Diagnostics.Debug.Assert(soundResource.FilePath != null, "Sound resource file path is null; cannot play it");

            PlaySound(soundResource.FilePath, isLooped);
        }

        public void PlayMusic(SoundResource soundResource, bool isLooped) {
            System.Diagnostics.Debug.Assert(soundResource != null, "Sound resource is null; cannot play it");
            System.Diagnostics.Debug.Assert(soundResource.FilePath != null, "Sound resource file path is null; cannot play it");

            PlayMusic(soundResource.FilePath, isLooped);
        }

        private void PlaySound(string filepath, bool isLooped) {
            ISound sound;
            ISoundSource soundSource = SoundEngine.GetSoundSource(filepath, true);
            soundSource.DefaultVolume = _soundVolume / 100.0f;

            if (isLooped)
                sound = SoundEngine.Play2D(soundSource, true, false, false);
            else
                sound = SoundEngine.Play2D(soundSource, false, false, false);

            FxControl = sound.SoundEffectControl;
        }

        private void PlayMusic(string filepath, bool isLooped) {
            if (_currentMusic != null) {
                _currentMusic.Stop();
            }

            if (isLooped) {
                _currentMusic = SoundEngine.Play2D(filepath, true, true, StreamMode.AutoDetect, false);
                _currentMusic.Volume = MusicVolume / 100.0f;
                _currentMusic.Paused = false;
            }
            else {
                _currentMusic = SoundEngine.Play2D(filepath, false, true, StreamMode.AutoDetect, false);
                _currentMusic.Volume = MusicVolume / 100.0f;
                _currentMusic.Paused = false;
            }
        }

        public bool IsSoundPlaying(string filepath) {
            if (SoundEngine.IsCurrentlyPlaying(filepath))
                return true;
            else
                return false;
        }

        public void DistortSound() {
            if (FxControl.IsDistortionSoundEffectEnabled)
                FxControl.DisableDistortionSoundEffect();
            else
                FxControl.EnableDistortionSoundEffect();
        }

        public void EchoSound() {

        }

        public void RevertSound() {
            if (FxControl.IsWavesReverbSoundEffectEnabled)
                FxControl.DisableWavesReverbSoundEffect();
            else
                FxControl.EnableWavesReverbSoundEffect();
        }

        public void StopAllSoundEffects() {
            FxControl.DisableAllEffects();
        }

        public void StopAllSounds() {
            SoundEngine.StopAllSounds();
        }
    }
}

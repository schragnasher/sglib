﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.EntitySystem {

    public abstract class Component {

        public IGame Game { get; set; }
        public Entity Entity { get; set; }
        public bool Enabled { get; protected set; }
        public bool Started { get; protected set; }

        protected Component() {
            Enabled = true;
            Started = false;
        }

        public void Destroy() {
            Entity.RemoveComponent(this);
        }

        public abstract void Initialize();
        public abstract void Startup();
        public abstract void Update(float deltaTime);
        public abstract void Shutdown();
        public abstract void Enable();
        public abstract void Disable();

    }

}

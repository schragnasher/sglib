﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.EntitySystem {
    public interface IEntityManagerService {
        Entity CreateEntity(string entityName);
        Entity CreateEntity(string entityName, string entityTag);
        Entity CreateEntity(string entityName, int layer);
        Entity CreateEntity(string entityName, string entityTag, int layer);
        Entity GetEntity(string entityId);
        Entity FindEntityWithTag(string tag);
        List<Entity> FindEntitiesWithTag(string tag);
        void ClearEntities();
    }
}

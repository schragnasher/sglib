﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleGamesLibrary.EntitySystem {

    public class Entity {

        public IGame Game { get; set; }
        public EntityManager EntityManager { get; set; }
        public string Id { get; set; }
        public string Tag { get; set; }
        public int Layer { get; private set; }
        public bool Enabled { get; private set; }

        private readonly Dictionary<Type, Component> _components = new Dictionary<Type, Component>();
        private List<Component> _updateComponents;

        public event EventHandler<AttachEventArgs> AddComponentEvent;
        public event EventHandler<RemoveEventArgs> RemoveComponentEvent;

        #region Constructors
        public Entity(string entityId) :
            this(entityId, "") {
        }

        public Entity(string entityId, string entityTag) :
            this(entityId, "", 0) {
        }

        public Entity(string entityId, int layer) :
            this(entityId, "", layer) {
        }

        public Entity(string entityId, string entityTag, int layer) {
            Enabled = true;
            Id = entityId;
            Tag = entityTag;
            Layer = layer;
        }
        #endregion

        public void Enable() {
            if (Enabled == false) {
                Enabled = true;
                foreach (Component component in _components.Values) {
                    component.Enable();
                }
            }
        }

        public void Disable() {
            if (Enabled) {
                Enabled = false;
                foreach (Component component in _components.Values) {
                    component.Disable();
                }
            }
        }

        protected void OnAddComponent(Component component) {
            if (AddComponentEvent != null)
                AddComponentEvent(this, new AttachEventArgs(component));
        }

        protected void OnRemoveComponent(Component component) {
            if (RemoveComponentEvent != null)
                RemoveComponentEvent(this, new RemoveEventArgs(component));
        }

        public void Update(float deltaTime) {
            _updateComponents = _components.Values.ToList<Component>();

            foreach (Component component in _updateComponents) {
                if (!component.Started) {
                    component.Startup();
                }
            }

            foreach (Component component in _updateComponents) {
                if (component.Enabled) {
                    component.Update(deltaTime);
                }
            }
        }

        public T AddComponent<T>() where T : Component, new() {
            Component component;
            if (_components.TryGetValue(typeof(T), out component)) { 
                return (T)component; 
            }
            
            component = new T();
            component.Entity = this;
            component.Game = Game;

            _components.Add(component.GetType(), component);
            component.Initialize();
            OnAddComponent(component);
            
            return (T)component;
        }

        public void RemoveComponent<T>() where T : Component {
            System.Diagnostics.Debug.Assert(_components.ContainsKey(typeof(T)),
                String.Format("Request to remove component type {0} from entity {1} failed.",typeof(T), Id));
            _components[typeof(T)].Shutdown();
            _components.Remove(typeof(T));
        }

        public void RemoveComponent(Component component) {
            System.Diagnostics.Debug.Assert(_components.Values.Contains(component),
                String.Format("Request to remove component {0} from entity {1} failed.",
                component, Id));
            _components[component.GetType()].Shutdown();
            _components.Remove(component.GetType());
        }

        public T GetComponent<T>() where T : Component {
            if (!_components.ContainsKey(typeof(T)))
                return null;
            else
                return _components[typeof(T)] as T;
        }

        public void Shutdown() {
            _updateComponents = _components.Values.ToList<Component>();
            foreach (Component component in _updateComponents) {
                component.Destroy();
            }
            _updateComponents.Clear();
        }

        public void Destroy() {
            EntityManager.RemoveEntity(this);
        }

    }

    public class AttachEventArgs : EventArgs {
        public Component Component { get; private set; }

        public AttachEventArgs(Component component) { 
            Component = component;
        }
    }

    public class RemoveEventArgs : EventArgs {
        public Component Component { get; private set; }

        public RemoveEventArgs(Component component) {
            Component = component;
        }
    }

}

﻿using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.SceneSystem;
using SlimDX;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.EntitySystem {

    public class TextComponent : Component {

        private TransformComponent _transformComponent;
        private TextNode _textNode;

        public Color4 Color {
            get { return _textNode.Color; }
            set { _textNode.Color = value; }
        }

        public FontResource Font {
            get { return _textNode.Font; }
            set { _textNode.Font = value; }
        }

        public string Text {
            get { return _textNode.Text; }
            set { _textNode.Text = value; }
        }

        public DrawTextFormat Format {
            get { return _textNode.Format; }
            set { _textNode.Format = value; } 
        }

        public TextComponent() {
            _textNode = new TextNode();
        }

        public override void Initialize() {
            _transformComponent = Entity.GetComponent<TransformComponent>();
            if (_transformComponent == null) {
                _transformComponent = Entity.AddComponent<TransformComponent>();
            }
            _transformComponent.AddChildNode(_textNode);

            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent += ComponentRemovedFromEntityHandler;
        }

        public override void Startup() {
            Started = true;
        }

        public override void Update(float deltaTime) {

        }

        public override void Enable() {
            if (!Enabled && _transformComponent != null) {
                _transformComponent.AddChildNode(_textNode);
            }
            Enabled = true;
        }

        public override void Disable() {
            if (Enabled && _transformComponent != null) {
                _transformComponent.RemoveChildNode(_textNode);
            }
            Enabled = false;
        }

        public override void Shutdown() {
            Entity.AddComponentEvent -= ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent -= ComponentRemovedFromEntityHandler;
            _textNode.Destroy();
        }


        #region Events
        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = attachEventArgs.Component as TransformComponent;
                if (Enabled) {
                    _transformComponent.AddChildNode(_textNode);
                }

            }
        }

        public void ComponentRemovedFromEntityHandler(object sender, RemoveEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = null;
                if (_textNode.Parent != null) {
                    _textNode.Parent.Remove(_textNode);
                }
            }
        }
        #endregion
    }
}

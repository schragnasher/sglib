﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.Utilities;
using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.AudioSystem;
using SimpleGamesLibrary.LevelSystem;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.SceneSystem;

namespace SimpleGamesLibrary.EntitySystem {

    public abstract class ScriptComponent : Component {

        protected TransformComponent Transform { get; set; }
        protected CameraComponent Camera { get; set; }
        protected SpriteComponent Sprite { get; set; }

        protected IGUIManagerService GuiManager;
        protected IEntityManagerService EntityManager;
        protected IAudioService AudioManager;
        protected ILevelManagerService LevelManager;
        protected ISceneManagerService SceneManager;

        protected ResourceManager<TextureResource, TextureResourceDescriptor> TextureResourceManager;
        protected ResourceManager<FontResource, FontResourceDescriptor> FontResourceManager;
        protected ResourceManager<SoundResource, SoundResourceDescriptor> SoundResourceManager;

        protected ScriptComponent() {

        }

        public override void Initialize() {
            TextureResourceManager = Game.TextureResourceManager;
            FontResourceManager = Game.FontResourceManager;
            SoundResourceManager = Game.SoundResourceManager;

            GuiManager = Game.GameServices.GetService<IGUIManagerService>();
            EntityManager = Game.GameServices.GetService<IEntityManagerService>();
            AudioManager = Game.GameServices.GetService<IAudioService>();
            LevelManager = Game.GameServices.GetService<ILevelManagerService>();
            SceneManager = Game.GameServices.GetService<ISceneManagerService>();

            Transform = Entity.GetComponent<TransformComponent>();
            Camera = Entity.GetComponent<CameraComponent>();
            Sprite = Entity.GetComponent<SpriteComponent>();

            Awaken();
        }

        public override void Startup() {
            Start();
            Started = true;
        }

        public override void Shutdown() {
            OnDestroy();
        }

        public override void Enable(){
            Enabled = true;
        }

        public override void Disable() {
            Enabled = false;
        }

        public virtual void Awaken() { }
        public virtual void Start() { Started = true; }
        public abstract override void Update(float deltaTime);
        public virtual void OnEnable() { }
        public virtual void OnDisable() { }
        public virtual void OnDestroy() { }

    }

}

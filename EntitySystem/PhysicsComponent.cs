﻿using System.Collections.Generic;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.PhysicsSystem;
using SimpleGamesLibrary.Utilities;

namespace SimpleGamesLibrary.EntitySystem {
    public class PhysicsComponent : Component {
        private Fixture _fixture;
        private List<Fixture> _fixtureCollection;

        private IPhysicsManagerService PhysicsManager;
        private TransformComponent Transform;

        private Body _physicsBody;
        private Shape _collisionShape;

        #region Component Members
        public override void Initialize() {
            PhysicsManager = Game.GameServices.GetService<IPhysicsManagerService>();
            Transform = Entity.AddComponent<TransformComponent>();
        }

        public override void Startup() {
        
        }

        public override void Update(float deltaTime) {
            //System.Diagnostics.Debug.Assert(_fixture != null, string.Format("Entity \"{0}\" has a physics component but doesn't have a body attached. Can't update() physics !", Entity.Id));

            if (_fixture != null) {
                Transform.Rotate(_fixture.Body.Rotation - Transform.CurrentRotation);
                Transform.Translate(
                    ConvertUnits.ToDisplayUnits(_fixture.Body.Position.X) - Transform.CurrentPosition.X,
                    ConvertUnits.ToDisplayUnits(_fixture.Body.Position.Y) - Transform.CurrentPosition.Y
                );
            }
        }

        public override void Enable() {}

        // Note: When a body is removed from the world, it gets disassembled
        // and is essentially worthless. To put back in world, have to 
        // reconstruct all.
        public override void Disable() {
            if (_fixture != null && _fixture.Body != null)
                PhysicsManager.World.RemoveBody(_fixture.Body);

            _fixture = null;
        }

        //TODO: Is this proper?
        public override void Shutdown() {
            if (_fixture != null && _fixture.Body != null)
                PhysicsManager.World.RemoveBody(_fixture.Body);

            _fixture = null;

        }
        #endregion

       #region Body/Shape/Fixture Construction

        public void CreateRectangleBody(int pixelWidth, int pixelHeight, float density, BodyType bodyType = BodyType.Dynamic) {
            _fixture = FixtureFactory.AttachRectangle(
                ConvertUnits.ToSimUnits(pixelWidth),
                ConvertUnits.ToSimUnits(pixelHeight),
                density,  // The shape's density, usually in kg/m^2
                new Vector2(0, 0),
                BodyFactory.CreateBody(PhysicsManager.World, ConvertUnits.ToSimUnits(Transform.CurrentPosition)),
                Entity);

            _fixture.Body.BodyType = bodyType;
            _fixture.Body.Rotation = Transform.CurrentRotation;

            _fixture.Body.OnCollision += OnCollision;
        }

        #endregion 

        #region Collision

        public event OnCollisionEventHandler CollisionEvent;

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact) {
            if (CollisionEvent != null)
                return CollisionEvent(fixtureA, fixtureB, contact);
            else
                return true;
        }

        public void SetCollisionCategory(Category collisionCategory) {
            _fixture.Body.CollisionCategories = collisionCategory;
        }

        public void SetCollidesWith(Category categoriesToCollideWith) {
            _fixture.Body.CollidesWith = categoriesToCollideWith;
        }

        #endregion

        public void SetPosition(Vector2 position) {
            Transform.SetPosition(position);
            _fixture.Body.SetTransform(ConvertUnits.ToSimUnits(position), _fixture.Body.Rotation);
        }

        public Vector2 CurrentPositionPixels() {
            return Transform.CurrentPosition;
        }

        /// <remarks>
        /// Force is in NEWTONS
        /// One newton is the force required to accelerate one kilogram at one meter per second per second (kg x m/s^2).
        /// On earth, a one-kilogram object has a weight of 9.8 newtons.
        /// </remarks>
        public void ApplyForce(Vector2 force) {
            _fixture.Body.ApplyForce(force);
        }

        public void ApplyForce(float xForce, float yForce) {
            ApplyForce(new Vector2(xForce, yForce));
        }

        #region Apply physics
        /// <summary>
        /// Apply rotational force (in radians)
        /// </summary> 
        //Note: Is in N-m
        public void ApplyTorqueRadians(float radians) {
            _fixture.Body.ApplyTorque(radians);
        }

        /// <summary>
        /// Apply rotational force (in degrees)
        /// </summary> 
        public void ApplyTorqueDegrees(float degrees) {
            ApplyTorqueRadians(MathHelper.ToRadians(degrees));
        }

        /// <summary>
        /// Apply quick linear velocity change (no acceleration)
        /// </summary>
        // Note: Usually in N-seconds or kg-m/s
        public void ApplyLinearImpulse(Vector2 impulse) {
            _fixture.Body.ApplyLinearImpulse(impulse);
        }

        /// <summary>
        /// Apply quick angular velocity change (no acceleration)
        /// </summary>
        // Note: Is in kg*m*m/s
        public void ApplyAngularImpulse(float impulse) {
            _fixture.Body.ApplyAngularImpulse(impulse);
        }

        public float CurrentRotation() {
            return _fixture.Body.Rotation;
        }

        // Note: Is in radians/second
        public float AngularVelocity {
            set {   _fixture.Body.AngularVelocity = value; }
            get { return _fixture.Body.AngularVelocity; }
        }

        public Vector2 LinearVelocity {
            set { _fixture.Body.LinearVelocity = value; }
            get { return _fixture.Body.LinearVelocity; }
        }

        #endregion
    }
}
  
﻿using SimpleGamesLibrary.SceneSystem;

namespace SimpleGamesLibrary.EntitySystem {

    public class CameraComponent : Component {

        private TransformComponent _transformComponent;
        private readonly CameraNode _cameraNode;
        private ISceneManagerService SceneManager;
        
        public CameraComponent() : base() {
            _cameraNode = new CameraNode();
        }

        public override void Initialize() {
            SceneManager = Game.GameServices.GetService<ISceneManagerService>();
            _transformComponent = Entity.GetComponent<TransformComponent>();
            if (_transformComponent != null) {
                _transformComponent.AddChildNode(_cameraNode);
                Enable();
            }
            else {
                Disable();
            }
            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent += ComponentRemovedFromEntityHandler;
        }

        public override void Startup() {
            Started = true;
        }

        public override void Update(float deltaTime) {
            if (Enabled && SceneManager.ActiveCamera != _cameraNode) { Disable(); }
        }

        public override void Shutdown() {
            _cameraNode.Destroy();
        }

        public override void Enable() {
            SceneManager.ActiveCamera = _cameraNode;
            Enabled = true;
        }

        public override void Disable() {
            if (SceneManager.ActiveCamera == _cameraNode) { SceneManager.ActiveCamera = null; }
            Enabled = false;
        }

        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = attachEventArgs.Component as TransformComponent;
                _transformComponent.AddChildNode(_cameraNode);
            }
        }

        public void ComponentRemovedFromEntityHandler(object sender, RemoveEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                if (_transformComponent != null) {
                    Disable();
                }
            }
        }

    }

}

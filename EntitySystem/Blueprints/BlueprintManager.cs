﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.PhysicsSystem;
using SimpleGamesLibrary.ResourceSystem;
using System.Xml.Linq;
using System.Reflection;

namespace SimpleGamesLibrary.EntitySystem.Blueprints {
    public class BlueprintManager : IBlueprintManagerService {
        private const string xmlRootPath = @".\Content\Blueprints";

        private IGame Game;
        private Dictionary<string, Blueprint> blueprints;

        public BlueprintManager(IGame game) {
            Game = game;
            Game.GameServices.RegisterService<IBlueprintManagerService>(this);

            blueprints = new Dictionary<string, Blueprint>();

            // TEST 
            AddBlueprint("bullet", new Blueprint("bullet", "Bullet.xml"));
        }

        public void AddBlueprint(string blueprintId, Blueprint blueprint) {
            blueprints.Add("bullet", new Blueprint(blueprintId, "Bullet.xml"));
        }

        public Entity Initialize(string blueprintId, string entityName, int xPosition = 0, int yPosition = 0, float rotation = 0) {
            System.Diagnostics.Debug.Assert(blueprints.ContainsKey(blueprintId), String.Format("Blueprint id {0} not found.", blueprintId));

            return EntityFactory(blueprints[blueprintId], entityName, xPosition, yPosition, rotation);
        }

        private Entity EntityFactory(Blueprint blueprint, string entityName, int xPosition, int yPosition, float rotation) {
            XDocument blueprintsDocument = XDocument.Load(Path.Combine(xmlRootPath, blueprint.XmlFileName));

            Entity entity = Game.GameServices.GetService<IEntityManagerService>().CreateEntity(entityName);

            // Transform Component
            TransformComponent transform = entity.AddComponent<TransformComponent>();
            if (xPosition != 0 && yPosition != 0) transform.SetPosition(xPosition, yPosition);
            if (rotation != 0) transform.SetRotation(rotation);

            // Physics Component
            var hasPhysics = (
                           from xmlBlueprints in blueprintsDocument.Descendants()
                           where (string)xmlBlueprints.Attribute("Id") == blueprint.XmlId
                           select xmlBlueprints.Elements("PhysicsComponent").Any()).Single();

            if (hasPhysics)
                entity.AddComponent<PhysicsComponent>();

            // Sprite Component
            IEnumerable<string> sprite = (
                from xmlBlueprints in blueprintsDocument.Descendants()
                where (string)xmlBlueprints.Attribute("Id") == blueprint.XmlId
                && xmlBlueprints.Element("SpriteComponent") != null
                select (string)xmlBlueprints.Element("SpriteComponent").Attribute("Path"));

            if (sprite != null) {
                string spritePath = "NULL";

                foreach (string xmlSpritePath in sprite)
                    spritePath = xmlSpritePath;

                SpriteComponent spriteComponent = entity.AddComponent<SpriteComponent>();
                spriteComponent.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor(spritePath, Game.GraphicsDevice.Device));
            }

            //// Script Component
            //IEnumerable<string> script = (
            //    from xmlBlueprints in blueprintsDocument.Descendants()
            //    where (string)xmlBlueprints.Attribute("Id") == "bullet"
            //    && xmlBlueprints.Element("ScriptComponent") != null
            //    select (string)xmlBlueprints.Element("ScriptComponent").Attribute("Name"));

            //if (script != null) {
            //    string scriptName = "NULL";

            //    foreach (string xmlScriptName in script)
            //        scriptName = xmlScriptName;

            //  //object scriptInstance = Activator.CreateInstance(Assembly.GetEntryAssembly().GetType("TestGame.Scripts.PhysicsLevel."+"BulletScript"));
            //  //Type scriptType = scriptInstance.GetType();
            //      Type type = Type.GetType("TestGame.Scripts.PhysicsLevel."+"BulletScript");
            //    //Type type2 = type.MakeGenericType(type);
            //  //entity.AddComponent<TestGame.Scripts.PhysicsLevel.BulletScript>();

            //  //System.Console.WriteLine("Running assembly: " + Assembly.GetEntryAssembly().GetType("TestGame.Scripts.PhysicsLevel.BulletScript", true, true));
            //}


            //SpriteComponent sprite = null;
            //if (blueprint.HasSprite) {
            //    sprite = entity.AddComponent<SpriteComponent>();
            //    sprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor(blueprint.GetSpritePath(), Game.GraphicsDevice.Device));
            //}

            //if (blueprint.HasPhysics) {
            //    PhysicsComponent physics = entity.AddComponent<PhysicsComponent>();
            //    physics.CreateRectangleBody(sprite.Texture.Info.Width, sprite.Texture.Info.Height, 1.0f);
            //}

            return entity;
        }
    }
    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.EntitySystem.Blueprints {
    public class Blueprint {
        public Blueprint(string xmlId, string xmlName) {
            XmlId = xmlId;
            XmlFileName = xmlName;
        }
        public string XmlId { get; set; }
        public string XmlFileName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FarseerPhysics.Xna;

namespace SimpleGamesLibrary.EntitySystem.Blueprints {
    public interface IBlueprintManagerService {
        Entity Initialize(string blueprintId, string entityName, int xPosition = 0, int yPosition = 0, float rotation = 0);
    }
}

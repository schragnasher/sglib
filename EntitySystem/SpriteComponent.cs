﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.SceneSystem;
using SimpleGamesLibrary.ResourceSystem;
using System.IO;
using Color4 = SlimDX.Color4;
using Vector2 = FarseerPhysics.Xna.Vector2;

namespace SimpleGamesLibrary.EntitySystem {

    public class SpriteComponent : Component {

        public Color4 Color {
            get {
                return _spriteNode.Color;
            }
            set {
                _spriteNode.Color = value;
           } 
        }

        public TextureResource Texture { 
            get { return _spriteNode.Texture; }
            set {_spriteNode.Texture = value; }
        } 

        public Rectangle SubTexture { 
            get { return _spriteNode.SubTexture; } 
            set { _spriteNode.SubTexture = value; } 
        }

        public Vector2 Origin {
            get { return _spriteNode.Origin; }
            set { _spriteNode.Origin = value; }
        }
        
        private SpriteNode _spriteNode;
        private TransformComponent _transformComponent;

        public SpriteComponent() {
            _spriteNode = new SpriteNode();
        }

        public override void Initialize() {
            _transformComponent = Entity.AddComponent<TransformComponent>();
            if (Enabled) {
                _transformComponent.AddChildNode(_spriteNode);
            }

            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent += ComponentRemovedFromEntityHandler;
        }

        public override void Startup() {
            Started = true;
        }

        public override void Shutdown() {
            Entity.AddComponentEvent -= ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent -= ComponentRemovedFromEntityHandler;
            _spriteNode.Destroy();
        }

        public override void Update(float deltaTime) {
            // do nothing
        }

        public override void Enable() {
            if (!Enabled && _transformComponent != null) {
                _transformComponent.AddChildNode(_spriteNode);
            }
            Enabled = true;
        }

        public override void Disable() {
            if (Enabled && _transformComponent != null) {
                _transformComponent.RemoveChildNode(_spriteNode);
            }
            Enabled = false;
        }

        #region Events
        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = attachEventArgs.Component as TransformComponent;

                if (Enabled && _spriteNode.Parent == null)
                    _transformComponent.AddChildNode(_spriteNode);
                    
            }
        }

        public void ComponentRemovedFromEntityHandler(object sender, RemoveEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = null;
            }

            if (Enabled && _spriteNode.Parent != null)
                _spriteNode.Parent.Remove(_spriteNode);
        }
        #endregion
    }
}

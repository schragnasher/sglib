﻿using System;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.SceneSystem;

namespace SimpleGamesLibrary.EntitySystem {

    public class TransformComponent : Component {

        private readonly TransformNode _transformNode = new TransformNode();
        private ISceneManagerService SceneManager;

        public Vector2 CurrentScale {
            get {
                return _transformNode.Scale;
            }
        }

        public Vector2 CurrentPosition {
            get {
                return _transformNode.Position;
            }
        }

        public float CurrentRotation {
            get { return _transformNode.Rotation; }
        }

        private TransformComponent _parent = null;
        public TransformComponent Parent {
            get { return _parent; }
            set {
                if (_parent != null) {
                    _parent.RemoveChildNode(_transformNode);
                }

                if (value == null) {
                    SceneManager.AddNode(_transformNode, Entity.Layer);
                }
                else {
                    if (_transformNode.Parent != null)
                        _transformNode.Parent.Remove(_transformNode);

                    value.AddChildNode(_transformNode);
                    _parent = value;
                }

            }
        }


        public override void Initialize() {
            SceneManager = Game.GameServices.GetService<ISceneManagerService>();
            SceneManager.AddNode(_transformNode, Entity.Layer);
        }

        public override void Startup() {

        }

        public override void Shutdown() {
            _transformNode.Destroy();
            Started = true;
        }

        public void AddChildNode(Node child) {
            System.Diagnostics.Debug.Assert(!_transformNode.children.Contains(child), String.Format("Entity {0}'s transform node already contains child {1}", this.Entity.Id, child.ToString()));
            _transformNode.Add(child);
        }

        public void RemoveChildNode(Node child) {
            System.Diagnostics.Debug.Assert(_transformNode.children.Contains(child), String.Format("Entity {0}'s transform node doesn't contain child {1}", this.Entity.Id, child.ToString()));

            _transformNode.Remove(child);
        }

        public override void Update(float deltaTime) {
            // do nothing
        }

        public override void Enable() {
            // do nothing
        }

        public override void Disable() {
            // do nothing
        }

        public void SetPosition(float x, float y) {
            SetPosition(new Vector2(x, y));
        }

        public void SetPosition(Vector2 position) {
            _transformNode.SetPosition(position);
        }

        public void SetRotation(float radians) {
            _transformNode.SetRotation(radians);
        }

        public void SetScale(float xScale, float yScale) {
            SetScale(new Vector2(xScale, yScale));
        }

        public void SetScale(Vector2 scale) {
            _transformNode.SetScale(scale);
        }

        public void Translate(float x, float y) {
            Translate(new Vector2(x, y));
        }

        public void Translate(Vector2 position) {
             _transformNode.Translate(position);
        }

        public void Rotate(float radians) {
            _transformNode.Rotate(radians);
        }

        public void Scaling(float x, float y) {
            Scaling(new Vector2(x, y));
        }

        public void Scaling(Vector2 scale) {
            _transformNode.Scaling(scale);
        }

    }

}

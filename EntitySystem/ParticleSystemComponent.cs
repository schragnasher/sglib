﻿using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.SceneSystem;
using SlimDX;
using Vector2 = FarseerPhysics.Xna.Vector2;


namespace SimpleGamesLibrary.EntitySystem {
    public class ParticleSystemComponent : Component {
        private ParticleSystemNode _particleSystemNode;
        private TransformComponent _transformComponent;

        public ParticleSystemComponent() {
            _particleSystemNode = new ParticleSystemNode();
        }

        #region Properties
        public bool EmittorActive { 
            get { return _particleSystemNode.EmittorActive; }
            set { _particleSystemNode.EmittorActive = value; }
        }
        public bool OneShot { 
            get { return _particleSystemNode.OneShot; } 
            set { _particleSystemNode.OneShot = value; }
        }
        public Vector2 StartVelocity {
            get { return _particleSystemNode.StartVelocity; }
            set { _particleSystemNode.StartVelocity = value; }
        }
        public Vector2 RandVelocity {
            get { return _particleSystemNode.RandVelocity; }
            set { _particleSystemNode.RandVelocity = value; }
        }
        public float MinStartingSize {
            get { return _particleSystemNode.MinStartingSize; }
            set { _particleSystemNode.MinStartingSize = value; }
        }
        public float MaxStartingSize {
            get { return _particleSystemNode.MaxStartingSize; }
            set { _particleSystemNode.MaxStartingSize = value; }
        }
        public float MinLifetime {
            get { return _particleSystemNode.MinLifetime; }
            set { _particleSystemNode.MinLifetime = value; }
        }
        public float MaxLifetime {
            get { return _particleSystemNode.MaxLifetime; }
            set { _particleSystemNode.MaxLifetime = value; }
        }
        public float MinEmission {
            get { return _particleSystemNode.MinEmission; }
            set { _particleSystemNode.MinEmission = value; }
        }
        public float MaxEmission {
            get { return _particleSystemNode.MaxEmission; }
            set { _particleSystemNode.MaxEmission = value; }
        }
        public float TimeToNextEmission {
            get { return _particleSystemNode.TimeToNextEmission; }
            set { _particleSystemNode.TimeToNextEmission = value; }
        }
        public Color4 StartColor {
            get { return _particleSystemNode.StartColor; }
            set { _particleSystemNode.StartColor = value; }
        }
        public Color4 EndColor {
            get { return _particleSystemNode.EndColor; }
            set { _particleSystemNode.EndColor = value; }
        }
        public TextureResource Texture {
            get { return _particleSystemNode.Texture; }
            set { _particleSystemNode.Texture = value; }
        }

        public bool DestroyWhenEmittorDone {
            get;
            set; 
        }
        #endregion

        public override void Initialize() {
            _transformComponent = Entity.AddComponent<TransformComponent>();
            if (Enabled) {
                _transformComponent.AddChildNode(_particleSystemNode);
            }

            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent += ComponentRemovedFromEntityHandler;
        }

        public override void Startup() {
            Started = true;
        }

        public override void Update(float deltaTime) {
            if (DestroyWhenEmittorDone && _particleSystemNode.EmittorDone)
                Entity.Destroy();
        }

         public override void Shutdown() {
            Entity.AddComponentEvent -= ComponentAddedToEntityHandler;
            Entity.RemoveComponentEvent -= ComponentRemovedFromEntityHandler;
            _particleSystemNode.Destroy();
        }

        public override void Enable() {
            if (!Enabled && _transformComponent != null) {
                _transformComponent.AddChildNode(_particleSystemNode);
            }
            Enabled = true;
        }

        public override void Disable() {
            if (Enabled && _transformComponent != null) {
                _transformComponent.RemoveChildNode(_particleSystemNode);
            }
            Enabled = false;
        }

        #region Events
        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = attachEventArgs.Component as TransformComponent;

                if (Enabled && _particleSystemNode.Parent == null)
                    _transformComponent.AddChildNode(_particleSystemNode);
                    
            }
        }

        public void ComponentRemovedFromEntityHandler(object sender, RemoveEventArgs attachEventArgs) {
            if (attachEventArgs.Component is TransformComponent) {
                _transformComponent = null;
            }

            if (Enabled && _particleSystemNode.Parent != null)
                _particleSystemNode.Parent.Remove(_particleSystemNode);
        }
        #endregion
    }
}

﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
#endregion

namespace SimpleGamesLibrary.EntitySystem {

    public class EntityManager : IEntityManagerService {

        private readonly IGame Game;
        private readonly Dictionary<string, Entity> _entities = new Dictionary<string, Entity>();
        private List<Entity> _updateEntities;

        #region Constructor
        public EntityManager(IGame game) {
            Game = game;
            Game.GameServices.RegisterService<IEntityManagerService>(this);
        }
        #endregion

        public void Update(float deltaTime) {
            _updateEntities = _entities.Values.ToList<Entity>();
            foreach (Entity entity in _updateEntities) {
                if (entity.Enabled)
                    entity.Update(deltaTime);
            }
        }

        public void Initialize() { 
            
        }

        #region Entity Creation
        public Entity CreateEntity(string entityName) {
            return CreateEntity(entityName, "", 0);
        }

        public Entity CreateEntity(string entityName, string entityTag) {
            return CreateEntity(entityName, entityTag, 0);
        }

        public Entity CreateEntity(string entityName, int layer) {
            return CreateEntity(entityName, "", layer);
        }

        public Entity CreateEntity(string entityName, string entityTag, int layer) {
            Entity createdEntity = new Entity(entityName, entityTag, layer);
            createdEntity.Game = Game;
            createdEntity.EntityManager = this;
            _entities.Add(createdEntity.Id, createdEntity);
            return createdEntity;
        }
        #endregion

        #region Entity Removal
        public void RemoveEntity(string entityId) {
            if (_entities.ContainsKey(entityId)) {
                _entities[entityId].Shutdown();
                _entities.Remove(entityId);
            }
        }

        public void RemoveEntity(Entity entity) {
            if (_entities.ContainsKey(entity.Id)) {
                _entities[entity.Id].Shutdown();
                _entities.Remove(entity.Id);
            }
        }

        public void ClearEntities() {
            _updateEntities = _entities.Values.ToList<Entity>();
            foreach (var entity in _updateEntities) {
                RemoveEntity(entity);
            }
            _updateEntities.Clear();
        }
        #endregion

        #region Entity Retrevial
        public Entity GetEntity(string entityId) {
            if (_entities.ContainsKey(entityId))
                return _entities[entityId];
            else
                return null;
        }

        public Entity FindEntityWithTag(string tag) {
                var match = _entities.FirstOrDefault(pair => pair.Value.Tag == tag);

                return match.Value;
        }

        public List<Entity> FindEntitiesWithTag(string tag) {
            var matches = _entities.Where(pair => pair.Value.Tag == tag).ToList();

            if (matches.Count == 0)
                return null;
            else {
                List<Entity> entitiesWithTagCollection = new List<Entity>();

                foreach (var pair in matches)
                    entitiesWithTagCollection.Add(pair.Value);

                return entitiesWithTagCollection;
            }
        }
        #endregion

    }

}

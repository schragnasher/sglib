﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SlimDX;
using SlimDX.Direct3D9;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.ResourceSystem {

    public class TextureResource : IResource<TextureResourceDescriptor> {

        public Texture Texture { get; private set; }
        public ImageInformation Info { get; private set; }

        public void Load(TextureResourceDescriptor desc) {
            System.Diagnostics.Debug.Assert(File.Exists(desc.Filepath), string.Format("File path \"{0} doesn't exist\"", desc.Filepath));

            Info = ImageInformation.FromFile(desc.Filepath);
            Texture = Texture.FromFile(desc.Device, desc.Filepath, Info.Width, Info.Height, 1, Usage.None, Info.Format, Pool.Managed, Filter.Default, Filter.Linear, 0);
        }

        public void Dispose() {
            Texture.Dispose();
        }
    }

}

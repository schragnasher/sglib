﻿using System.Globalization;

namespace SimpleGamesLibrary.ResourceSystem {

    public class FontResourceDescriptor : IResourceDescriptor {

        public string Id { get; private set; }
        public string Filepath { get; set; }
        public GraphicsDevice GraphicsDevice { get; set; }
        public int Size { get; set; }

        public FontResourceDescriptor(string filepath, GraphicsDevice graphicsDevice, int size) {
            Id = filepath + "_" + size.ToString(CultureInfo.InvariantCulture);
            Filepath = filepath;
            GraphicsDevice = graphicsDevice;
            Size = size;
        }
        public void Dispose() { }
    }

}

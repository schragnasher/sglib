﻿using System;
using SlimDX.Direct3D9;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.ResourceSystem {
    public class TextureResourceDescriptor : IResourceDescriptor {
        public string Id { get; private set; }
        public string Filepath { get; private set; }
        public Device Device { get; private set; }

        public TextureResourceDescriptor(string filepath, Device device) {
            Id = filepath;
            Filepath = filepath;
            Device = device;
        }

        public void Dispose() { }
    }
}

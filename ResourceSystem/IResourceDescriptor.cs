﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.ResourceSystem {

    public interface IResourceDescriptor : IDisposable {
        string Id { get; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.ResourceSystem {
    
    public interface IResource<T> where T : IResourceDescriptor, IDisposable{

        void Load(T desc);
        void Dispose();
    }

}

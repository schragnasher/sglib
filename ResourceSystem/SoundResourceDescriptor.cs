﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.AudioSystem;

namespace SimpleGamesLibrary.ResourceSystem {

    public class SoundResourceDescriptor : IResourceDescriptor {

        public string Id { get; private set; }
        public string Filepath { get; private set; }
        public IAudioService AudioService { get; private set; }

        public SoundResourceDescriptor(string filepath, IAudioService audioService) {
            Id = filepath;
            Filepath = filepath;
            AudioService = AudioService;
        }
        public void Dispose() { }
    }

}

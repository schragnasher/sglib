﻿using System.IO;
using SlimDX.Direct3D9;
using System.Drawing.Text;
using System;

namespace SimpleGamesLibrary.ResourceSystem {

    public class FontResource : IResource<FontResourceDescriptor> {

        public GraphicsDevice GraphicsDevice { get; private set; }
        public Font Font { get; private set; }

        static private InstalledFontCollection installedFonts = new InstalledFontCollection();
        static private PrivateFontCollection privateFonts = new PrivateFontCollection();

        public void Load(FontResourceDescriptor desc) {
            string familyname = desc.Filepath;

            if (File.Exists(desc.Filepath)) {
                privateFonts.AddFontFile(desc.Filepath);
                familyname = privateFonts.Families[privateFonts.Families.Length - 1].Name;
            }

            GraphicsDevice = desc.GraphicsDevice;
            Font = new Font(
                GraphicsDevice.Device,
                desc.Size,
                0,
                FontWeight.Regular,
                0,
                false,
                CharacterSet.Default,
                Precision.TrueType,
                FontQuality.Antialiased,
                PitchAndFamily.Default,
                familyname
            );

            GraphicsDevice.DeviceLostEvent += DeviceLostHandler;
            GraphicsDevice.DeviceResetEvent += DeviceResetHandler;
        }

        public void DeviceLostHandler() {
            Font.OnLostDevice();
            Console.WriteLine("Font released");
        }
        public void DeviceResetHandler() {
            Font.OnResetDevice();
            Console.WriteLine("Font reset");
        }

        public void Dispose() {
            GraphicsDevice.DeviceLostEvent -= DeviceLostHandler;
            GraphicsDevice.DeviceResetEvent -= DeviceResetHandler;
        }

    }

}

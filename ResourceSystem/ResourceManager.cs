﻿using System;
using System.Collections.Generic;

namespace SimpleGamesLibrary.ResourceSystem {

    public class ResourceManager<T, D> : IDisposable
        where T : IResource<D>, new()
        where D : IResourceDescriptor {

        private Dictionary<string, IResource<D>> resources = new Dictionary<string, IResource<D>>();

        public ResourceManager() {
            
        }

        public T Load(D resourceDescriptor) {
            T resource = default(T);
            if (resources.ContainsKey(resourceDescriptor.Id)) { resource = (T)resources[resourceDescriptor.Id]; }
            else {
                resource = new T();
                resource.Load(resourceDescriptor);
                resources.Add(resourceDescriptor.Id, resource);
            }
            return resource;
        }

        public void Dispose() { 
            foreach(IResource<D> resource in resources.Values){
                resource.Dispose();
            }
        }
    }

}

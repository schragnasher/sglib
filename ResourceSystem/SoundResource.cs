﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.AudioSystem;
using System.IO;

namespace SimpleGamesLibrary.ResourceSystem {
    public class SoundResource : IResource<SoundResourceDescriptor> {
        public string FilePath { get; private set; }

        public void Load(SoundResourceDescriptor desc) {
            if (File.Exists(desc.Filepath)) {
                FilePath = desc.Filepath;
            }
        }

        public void Dispose() { }

    }

}

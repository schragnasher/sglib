﻿using System;
using SimpleGamesLibrary;
using SimpleGamesLibrary.EntitySystem;
using NUnit.Framework;

namespace TestGame {
    [TestFixture]
    public class EntityManagerTest {
        [Test]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNonExistingEntity() {
            Game game = new TestGame();
            var entityManager = game.GameServices.GetService<EntityManager>();
            entityManager.GetEntity("doesnt_exist");
        }

        [Test]
        public void GetExistingEntity() {
            Game game = new TestGame();
            var entityManager = game.GameServices.GetService<EntityManager>();
            entityManager.CreateEntity("exists");
            System.Diagnostics.Debug.Assert(entityManager.GetEntity("exists") != null);
        }
    }

    [TestFixture]
    public class ServiceProviderTest {
        [Test]
        public void RegisterService() {
            //GameServices gameServices = new GameServices();
            //AudioManager audioManager = new AudioManager();
            //gameServices.RegisterService<IAudioService>(audioManager);
        }

        [Test]
        public void GetExistingService() {
            //GameServices gameServices = new GameServices();
            //AudioManager audioManager = new AudioManager();
            //gameServices.RegisterService<IAudioService>(audioManager);

            //IAudioService audioservice;
            //Assert.IsNotNull(audioservice = gameServices.GetService<IAudioService>());
            //Assert.IsTrue(audioservice is IAudioService);
        }

        [Test]
        public void GetNonExistingService() {
            //GameServices gameServices = new GameServices();
            //Assert.IsNull(gameServices.GetService<IAudioService>());
        }

        [Test]
        public void RemoveExistingService() {
            //GameServices gameServices = new GameServices();
            //AudioManager audioManager = new AudioManager();
            //gameServices.RegisterService<IAudioService>(audioManager);
            //gameServices.RemoveService<IAudioService>();

            //Assert.IsNull(gameServices.GetService<IAudioService>());
        }
    }
}

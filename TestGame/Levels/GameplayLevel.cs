﻿using FarseerPhysics.Xna;
using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.LevelSystem;
using SimpleGamesLibrary;
using TestGame.Scripts;


namespace TestGame.Levels {
    public class GameplayLevel : Level {

        public GameplayLevel(IGame game)
            : base(game) {}

        public override void Load() {
            // GUI skin
            var mySkin = new GUISkin();
            mySkin.ButtonFont = FontResourceManager.Load(new FontResourceDescriptor("Content/Fonts/SCRIPTBL.ttf", Game.GraphicsDevice, 30));
            mySkin.ButtonDownImage = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/downbuttonimg.png", GraphicsDevice.Device));
            mySkin.ButtonOverImage = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/overbuttonimg.png", GraphicsDevice.Device));
            mySkin.ButtonUpImage = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/upbuttonimg.png", GraphicsDevice.Device));
            mySkin.SliderButton = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/sliderbutton.png", GraphicsDevice.Device));
            mySkin.SliderTrack = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/slidertrack.png", GraphicsDevice.Device));
            mySkin.LabelFont = FontResourceManager.Load(new FontResourceDescriptor("Content/Fonts/SCRIPTBL.ttf", Game.GraphicsDevice, 20));
            mySkin.CheckBoxChecked = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/checkboxchecked.png", GraphicsDevice.Device));
            mySkin.CheckBoxEmpty = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/checkboxempty.png", GraphicsDevice.Device));
            GuiManager.Skin = mySkin;

            // camera
            Entity cameraentity = EntityManager.CreateEntity("camera");
            cameraentity.AddComponent<TransformComponent>();
            cameraentity.AddComponent<CameraComponent>();
            CameraScript cameraScript = cameraentity.AddComponent<CameraScript>();
            cameraScript.Enable();

            // logo
            Entity logoentity = EntityManager.CreateEntity("logo");
            TransformComponent logoTransform = logoentity.AddComponent<TransformComponent>();
            logoTransform.SetScale(1f, 1f);
            SpriteComponent spriteComponent = logoentity.AddComponent<SpriteComponent>();
            spriteComponent.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/logo.png", Game.GraphicsDevice.Device));

            TestScriptComponent testScript = logoentity.AddComponent<TestScriptComponent>();
            testScript.Enable();

            // Gui 
            Entity guiEntity = EntityManager.CreateEntity("gui");
            guiEntity.AddComponent<GameplayGuiScript>();
            

            // text component
            Entity textEntity = EntityManager.CreateEntity("text",2);
            TransformComponent textTransform = textEntity.AddComponent<TransformComponent>();
            textTransform.SetPosition(new Vector2(100, 100));
            TextComponent textText = textEntity.AddComponent<TextComponent>();
            textText.Text = "HELLO WORLD!";
            textText.Font = FontResourceManager.Load(new FontResourceDescriptor("Content/Fonts/SCRIPTBL.ttf", Game.GraphicsDevice, 30));
        }

        public override void Unload() {

        }

    }
}

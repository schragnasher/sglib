﻿using FarseerPhysics.Xna;
using SimpleGamesLibrary;
using SimpleGamesLibrary.AudioSystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.LevelSystem;
using SimpleGamesLibrary.ResourceSystem;
using SlimDX;
using TestGame.Scripts;
using TestGame.Scripts.PhysicsLevel;
using Vector2 = FarseerPhysics.Xna.Vector2;

namespace TestGame.Levels {
    public class PhysicsLevel : Level {
        public PhysicsLevel(IGame game) : base(game) {}

        public override void Load() {
            // camera
            Entity cameraentity = EntityManager.CreateEntity("camera");
            cameraentity.AddComponent<TransformComponent>();
            cameraentity.AddComponent<CameraComponent>();
            CameraScript cameraScript = cameraentity.AddComponent<CameraScript>();
            cameraScript.Enable();

            // sound volume
            Game.GameServices.GetService<IAudioService>().SoundVolume = 30f;

            // background
            Entity background = EntityManager.CreateEntity("background", 0);
            background.AddComponent<TransformComponent>();
            SpriteComponent backgroundSprite = background.AddComponent<SpriteComponent>();
            backgroundSprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/Starfield.png", Game.GraphicsDevice.Device));

            // gui
            Entity gui = EntityManager.CreateEntity("gui");
            gui.AddComponent<PhysicsLevelGuiScript>();

            // spaceship
            Entity spaceshipEntity = EntityManager.CreateEntity("spaceship");
            TransformComponent spaceshipTransform = spaceshipEntity.AddComponent<TransformComponent>();
            SpriteComponent spaceshipSprite = spaceshipEntity.AddComponent<SpriteComponent>();
            PhysicsComponent spaceshipPhysics = spaceshipEntity.AddComponent<PhysicsComponent>();

            spaceshipTransform.SetRotation(MathHelper.ToRadians(90));
            spaceshipSprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/Player.png", Game.GraphicsDevice.Device));
            spaceshipPhysics.CreateRectangleBody(spaceshipSprite.Texture.Info.Width, spaceshipSprite.Texture.Info.Height, 5.0f);

            spaceshipEntity.AddComponent<SpaceshipScript>();

            // master logic
            Entity masterLogic = EntityManager.CreateEntity("gameLogic");
            masterLogic.AddComponent<AsteroidsScript>();
        }

        public override void Unload() {
        }
    }
}

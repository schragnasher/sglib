﻿using SimpleGamesLibrary;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.LevelSystem;
using TestGame.Scripts;
using TestGame.Scripts.MenuLevel;
using SimpleGamesLibrary.ResourceSystem;

namespace TestGame.Levels {
    public class MenuLevel : Level {
        public MenuLevel(IGame game) : base(game) {}

        public override void Load() {
            // camera
            Entity cameraentity = EntityManager.CreateEntity("camera");
            cameraentity.AddComponent<TransformComponent>();
            cameraentity.AddComponent<CameraComponent>();
            CameraScript cameraScript = cameraentity.AddComponent<CameraScript>();
            cameraScript.Enable();

            Entity menuGuiScript = EntityManager.CreateEntity("menuGuiScript");
            menuGuiScript.AddComponent<MenuLevelGuiScript>();
        }

        public override void Unload() {
            
        }
    }
}

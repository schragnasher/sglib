﻿using System;

namespace TestGame {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            using (TestGame game = new TestGame()) {
                game.Run();
            }
        }
    }
}

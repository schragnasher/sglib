﻿using SimpleGamesLibrary;
using TestGame.Levels;

namespace TestGame {

    public class TestGame : Game {
        public override void Update(float deltaTime) {

        }

        public override void Render(float ratioOfInterpolation) {

        } 

        public override void Load() {
            LevelManager.RegisterLevel("menuLevel", new MenuLevel(this));
            LevelManager.RegisterLevel("gameplayLevel", new GameplayLevel(this));
            LevelManager.RegisterLevel("physicsLevel", new PhysicsLevel(this));

            LevelManager.LoadLevel("menuLevel");

            InputManager.GetMouse().SetCursor("Content/Textures/cursor.ani");
        }

        public override void Unload() {
        
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.EntitySystem;

namespace TestGame.Scripts {
    public class StateMachine {
        private State _currentState;
        private State _previousState;

        private readonly Entity _entity;
        public StateMachine(Entity entity) {
            _entity = entity;
        }

        public void Update(float deltaTime) {
            if (_currentState != null)
                _currentState.Execute(_entity);
        }

        public void SetInitialState(State initialState) {
            _currentState = initialState;
            _currentState.StateMachine = this;
            _currentState.Enter(_entity);
        }

        public void ChangeCurrentState(State newState) {
            System.Diagnostics.Debug.Assert(newState != null, 
                String.Format("Trying to change to a null state: From {0} to {1} for entity {2}",
                _currentState, newState, _entity.Id));

            _previousState = _currentState;
            _currentState.Exit(_entity);

            _currentState = newState;
            _currentState.Enter(_entity);
        }

        public void RevertToPreviousState() {
            ChangeCurrentState(_previousState);
        }

        public bool IsInState<T>() where T: State {
            return typeof(T) == _currentState.GetType();
        }
    }
}

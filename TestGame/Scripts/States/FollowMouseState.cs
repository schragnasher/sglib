﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.InputSystem;

namespace TestGame.Scripts {
    class FollowMouseState : State {
        private IMouse _mouse;
        private Vector2 _mousePosition;

        private Entity _entity;

        public override void Enter(Entity entity) {
             _entity = entity;

             _mouse = entity.Game.GameServices.GetService<IInputManagerService>().GetMouse();
             //_mouse.MouseMoveEvent += MouseMoveEventHandler;
        }

        public override void Execute(Entity entity) {
            Console.WriteLine(string.Format("Following mouse! Location: {0}, {1}", 
                _entity.GetComponent<TransformComponent>().CurrentPosition.X, 
                _entity.GetComponent<TransformComponent>().CurrentPosition.Y));

            //_entity.GetComponent<TransformComponent>().Translate(Cursor.Position.X - 800, Cursor.Position.Y - 600);

            _entity.GetComponent<PhysicsComponent>().ApplyForce(
                new Vector2(_entity.GetComponent<TransformComponent>().CurrentPosition.X, _entity.GetComponent<TransformComponent>().CurrentPosition.Y) - new Vector2(Cursor.Position.X, Cursor.Position.Y));
        }

        public override void Exit(Entity entity) {
            Console.WriteLine("No longer following mouse!");
        }

    }
}

﻿using SimpleGamesLibrary.EntitySystem;

namespace TestGame.Scripts {
    public abstract class State {
        public StateMachine StateMachine { get; set; }

        public abstract void Enter(Entity entity);
        public abstract void Execute(Entity entity);
        public abstract void Exit(Entity entity);
    }
}

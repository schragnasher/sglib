﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Xna;
using SimpleGamesLibrary;
using SimpleGamesLibrary.AudioSystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.ResourceSystem;

namespace TestGame.Scripts.PhysicsLevel {
    public class BulletScript : ScriptComponent {
        private PhysicsComponent _physics;
        private SoundResource _explosionSound;

        float width;
        float height;
        float gameScreenWidth;
        float gameScreenHeight;

        public override void Awaken() {
            gameScreenWidth = Game.GraphicsDevice.ResolutionX;
            gameScreenHeight = Game.GraphicsDevice.ResolutionY;
        }

        public override void Start() {
            _physics = Entity.GetComponent<PhysicsComponent>();

            _physics.CollisionEvent += CollisionHandler;

            _explosionSound = Game.SoundResourceManager.Load(new SoundResourceDescriptor("Content/Sounds/Explosion.wav", Game.GameServices.GetService<IAudioService>()));

            width = Entity.GetComponent<SpriteComponent>().Texture.Info.Width;
            height = Entity.GetComponent<SpriteComponent>().Texture.Info.Height;
        }

        public bool CollisionHandler(Fixture f1, Fixture f2, Contact contact) {
            Entity.Disable();
            EntityManager.GetEntity("spaceship").GetComponent<SpaceshipScript>().shipAmmo.Add(Entity); // TODO: Bad way to do it, but works for now!
            AudioManager.PlaySound(_explosionSound, false);

            return true;
        }

        private void StayOnScreen() {
            Vector2 position = Entity.GetComponent<PhysicsComponent>().CurrentPositionPixels();

            if (position.X > (gameScreenWidth / 2 + (width / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)-Game.GraphicsDevice.ResolutionX / 2 - (width / 2), position.Y));
            else if (position.X < (-gameScreenWidth / 2 - (height / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)Game.GraphicsDevice.ResolutionX / 2 + (width / 2), position.Y));

            if (position.Y > (gameScreenHeight / 2 + (height / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2(position.X, (float)-Game.GraphicsDevice.ResolutionY / 2 - (height / 2)));
            else if (position.Y < (-gameScreenHeight / 2 - (height / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2(position.X, (float)Game.GraphicsDevice.ResolutionY / 2 + (height / 2)));
        }

        public override void Update(float deltaTime) {
            StayOnScreen();
        }

        public override void OnDestroy() {
            
        }
    }
}

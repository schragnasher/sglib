﻿using System;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.AudioSystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.PhysicsSystem;
using SimpleGamesLibrary.ResourceSystem;
using SlimDX;
using Vector2 = FarseerPhysics.Xna.Vector2;

namespace TestGame.Scripts.PhysicsLevel {
    public class AsteroidScript : ScriptComponent {
        private PhysicsComponent _physics;
        private SoundResource _explosionSound;

        public enum State { LargeAsteroid, MediumAsteroid, SmallAsteroid }
        public State CurrentState { get; set; }

        Random random;

        int gameScreenWidth;
        int gameScreenHeight;

        IPhysicsManagerService _physicsManager;

        public override void Awaken() {
            _physicsManager = Game.GameServices.GetService<IPhysicsManagerService>();

            gameScreenWidth = Game.GraphicsDevice.ResolutionX;
            gameScreenHeight = Game.GraphicsDevice.ResolutionY;

            random = new Random();
        }

        public override void Start() {
            _physics = Entity.GetComponent<PhysicsComponent>();
            _physics.CollisionEvent += CollisionHandler;
            _explosionSound = Game.SoundResourceManager.Load(new SoundResourceDescriptor("Content/Sounds/Explosion.wav", Game.GameServices.GetService<IAudioService>()));
        }

        public override void Update(float deltaTime) {
            TransformComponent transform = Entity.GetComponent<TransformComponent>();
            PhysicsComponent physics = Entity.GetComponent<PhysicsComponent>();
            float currentRotation = transform.CurrentRotation;

            physics.LinearVelocity = new Vector2(.5f * (float)Math.Cos(currentRotation), .5f * (float)Math.Sin(currentRotation));

            StayOnScreen(Entity);
        }


        public static int asteroidCounter;
        private const int asteroidsToCreate = 3;

        static int counter = 50;
        public bool CollisionHandler(Fixture f1, Fixture f2, Contact contact) {
            Entity particleTest = EntityManager.CreateEntity("particleTest" + ++counter, 7);
            TransformComponent particleTransform = particleTest.AddComponent<TransformComponent>();
            //particleTransform.Parent = Entity.GetComponent<TransformComponent>();
            particleTransform.SetPosition((f2.UserData as Entity).GetComponent<TransformComponent>().CurrentPosition);
            ParticleSystemComponent particleComponent = particleTest.AddComponent<ParticleSystemComponent>();
            particleComponent.Texture = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/SmokeParticle.png", Game.GraphicsDevice.Device));
            particleComponent.RandVelocity = new Vector2(5, 5);
            particleComponent.MinStartingSize = ((float)Entity.GetComponent<SpriteComponent>().Texture.Info.Width / particleComponent.Texture.Info.Width) - .5f;
            particleComponent.MaxStartingSize = ((float)Entity.GetComponent<SpriteComponent>().Texture.Info.Width / particleComponent.Texture.Info.Width) - .5f;
            particleComponent.MinLifetime = 0.3f;
            particleComponent.MaxLifetime = 0.5f;
            particleComponent.MinEmission = 100.0f;
            particleComponent.MaxEmission = 100.0f;
            particleComponent.StartColor = new Color4(0x545454);
            particleComponent.EndColor = new Color4(0x807C7A);
            particleComponent.EmittorActive = true;
            particleComponent.OneShot = true;

            // If a small asteroid, destroy it without creating any new asteroids
            if (CurrentState == State.SmallAsteroid) {
                Entity.Destroy();
                return true;
            }

            for (int x = 0; x < asteroidsToCreate; x++) {
                Entity asteroid = EntityManager.CreateEntity("asteroid" + asteroidCounter, "asteroids");
                var transform = asteroid.AddComponent<TransformComponent>();
                var sprite = asteroid.AddComponent<SpriteComponent>();
                var physics = asteroid.AddComponent<PhysicsComponent>();
                var script = asteroid.AddComponent<AsteroidScript>();

                transform.SetRotation(MathHelper.ToRadians(random.Next(0, 360)));
                transform.SetPosition(Entity.GetComponent<TransformComponent>().CurrentPosition);

                switch (CurrentState) {
                    case State.LargeAsteroid:
                        script.CurrentState = State.MediumAsteroid;
                        sprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/AsteroidM.png", Game.GraphicsDevice.Device));
                        break;
                    case State.MediumAsteroid:
                        script.CurrentState = State.SmallAsteroid;
                        sprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/AsteroidS.png", Game.GraphicsDevice.Device));
                        break;
                }

                physics.CreateRectangleBody(sprite.Texture.Info.Width, sprite.Texture.Info.Height, 10);
                physics.SetCollisionCategory(Category.Cat2); // asteroids
                physics.SetCollidesWith(Category.Cat1 | Category.Cat4); //spaceship and bullets

                asteroidCounter++;
            }

            Entity.Destroy();
            return true;
        }

        private void StayOnScreen(Entity asteroid) {
            Vector2 asteroidPosition = asteroid.GetComponent<TransformComponent>().CurrentPosition;
            float asteroidWidth = asteroid.GetComponent<SpriteComponent>().Texture.Info.Width;
            float asteroidHeight = asteroid.GetComponent<SpriteComponent>().Texture.Info.Height;

            if (asteroidPosition.X > ((float)gameScreenWidth / 2 + (asteroidWidth / 2)))
                asteroid.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)-Game.GraphicsDevice.ResolutionX / 2 - (asteroidWidth / 2), asteroidPosition.Y));
            else if (asteroidPosition.X < ((float)-gameScreenWidth / 2 - (asteroidWidth / 2)))
                asteroid.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)Game.GraphicsDevice.ResolutionX / 2 + (asteroidWidth / 2), asteroidPosition.Y));

            if (asteroidPosition.Y > ((float)gameScreenHeight / 2 + (asteroidHeight / 2)))
                asteroid.GetComponent<PhysicsComponent>().SetPosition(new Vector2(asteroidPosition.X, (float)-Game.GraphicsDevice.ResolutionY / 2 - (asteroidHeight / 2)));
            else if (asteroidPosition.Y < ((float)-gameScreenHeight / 2 - (asteroidHeight / 2)))
                asteroid.GetComponent<PhysicsComponent>().SetPosition(new Vector2(asteroidPosition.X, (float)Game.GraphicsDevice.ResolutionY / 2 + (asteroidHeight / 2)));
        }

        public override void OnDestroy() {}
    }
}

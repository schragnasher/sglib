﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using FarseerPhysics.Dynamics;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.EntitySystem.Blueprints;
using SimpleGamesLibrary.InputSystem;
using SimpleGamesLibrary.ResourceSystem;
using SlimDX;
using TestGame.Scripts.PhysicsLevel;
using Vector2 = FarseerPhysics.Xna.Vector2;

namespace TestGame.Scripts {
    public class SpaceshipScript : ScriptComponent{
        IBlueprintManagerService blueprintManager;

        PhysicsComponent Physics;

        StateMachine _stateMachine;
        private IKeyboard _keyboard;

        float shipWidth;
        float shipHeight;
        float gameScreenWidth;
        float gameScreenHeight;

        private const int maxBullets = 100;
        private Entity[] bullets;

        #region Script Members
        public override void Awaken() {
            blueprintManager = Game.GameServices.GetService<IBlueprintManagerService>();

            //_stateMachine = Entity.GetComponent<StateMachine>();
            //_stateMachine.SetInitialState(new FollowMouseState());

            gameScreenWidth = Game.GraphicsDevice.ResolutionX;
            gameScreenHeight = Game.GraphicsDevice.ResolutionY;

            _keyboard = Game.GameServices.GetService<IInputManagerService>().GetKeyboard();

            bullets = new Entity[maxBullets];
        }

        Entity engineParticle;
        public override void Start() {
            Physics = Entity.GetComponent<PhysicsComponent>();
            shipWidth = Entity.GetComponent<SpriteComponent>().Texture.Info.Width;
            shipHeight = Entity.GetComponent<SpriteComponent>().Texture.Info.Height;

            _keyboard.KeyDownEvent += KeyDownEventHandler;
            _keyboard.KeyUpEvent += KeyUpEventHandler;

            //Physics.SetCollisionCategory(Category.Cat1);
            //Physics.SetCollidesWith(Category.Cat2); // asteroids

            InitializeBullets();

            engineParticle = EntityManager.CreateEntity("engineParticle");

            TransformComponent particleTransform = engineParticle.AddComponent<TransformComponent>();
            TransformComponent shipTransform = Entity.GetComponent<TransformComponent>();
            particleTransform.Parent = Entity.GetComponent<TransformComponent>();
            particleTransform.SetPosition((shipTransform.CurrentPosition.X + (-30 * (float)Math.Cos(particleTransform.CurrentRotation))), shipTransform.CurrentPosition.Y + (-30 * (float)Math.Sin(particleTransform.CurrentRotation)));
            //particleTransform.SetRotation


            ParticleSystemComponent particleComponent = engineParticle.AddComponent<ParticleSystemComponent>();
            particleComponent.Texture = TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/redlaser.png", Game.GraphicsDevice.Device));
            //particleComponent.StartVelocity = spaceshipPhysics.Velocity + new Vector2(-250 * (float)Math.Cos(spatial.Rotation), -250 * (float)Math.Sin(spatial.Rotation));
            //particleComponent.RandVelocity = new Vector2(1, 1);
            //particleComponent.MinStartingSize = 3.0f;
            //particleComponent.MaxStartingSize = 3.0f;
            particleComponent.MinLifetime = 0.3f;
            particleComponent.MaxLifetime = 0.5f;
            particleComponent.MinEmission = 100.0f;
            particleComponent.MaxEmission = 100.0f;
            particleComponent.StartColor = new Color4(1, 1, 1, 1);
            particleComponent.EndColor = new Color4(1, 1, 1, 1);
            particleComponent.EmittorActive = true;
            particleComponent.OneShot = false;
        }

        public List<Entity> shipAmmo;
        public void InitializeBullets() {
            shipAmmo = new List<Entity>();

            int bulletCounter = 0;
            for (int x = 0; x < maxBullets; x++) {
                Entity bullet = blueprintManager.Initialize("bullet", "bullet" + bulletCounter);
                bullet.AddComponent<BulletScript>();
                bullet.Disable();

                shipAmmo.Add(bullet);
                bulletCounter++;
            }
        }

        public void Shoot() {
            Vector2 shipCurrentPosition = Entity.GetComponent<TransformComponent>().CurrentPosition;
            float shipCurrentRotation = Entity.GetComponent<TransformComponent>().CurrentRotation;

            Entity nextAvailableBullet = shipAmmo.FirstOrDefault(t => t != null);
            if (nextAvailableBullet != null) { // Ammo is available to use
                shipAmmo.Remove(nextAvailableBullet);
                nextAvailableBullet.Enable();

                TransformComponent bulletTransform = nextAvailableBullet.GetComponent<TransformComponent>();
                PhysicsComponent bulletPhysics = nextAvailableBullet.GetComponent<PhysicsComponent>();
                SpriteComponent bulletSprite = nextAvailableBullet.GetComponent<SpriteComponent>();

                bulletTransform.SetRotation(shipCurrentRotation);
                float bulletRotation = bulletTransform.CurrentRotation;
                bulletTransform.SetPosition(shipCurrentPosition.X + (30 * (float)Math.Cos(bulletRotation)), shipCurrentPosition.Y + (30 * (float)Math.Sin(bulletRotation)));

                bulletPhysics.CreateRectangleBody(bulletSprite.Texture.Info.Width, bulletSprite.Texture.Info.Height, 5.0f);
                bulletPhysics.SetCollisionCategory(Category.Cat4); // bullets
                bulletPhysics.SetCollidesWith(Category.Cat2); // asteroids
                bulletPhysics.ApplyLinearImpulse((new Vector2(1.0f * (float)Math.Cos(shipCurrentRotation), 1.0f * (float)Math.Sin(shipCurrentRotation))));
            }
            else {
                Console.WriteLine("OUT OF AMMO!");
                //TODO: Play "out of bullets" sound
            }
        }

        private float bulletTimeCounter = 0;
        private const float bulletTimeLimiter = .2f;
        public override void Update(float deltaTime) {
            UpdatePhysics();

            // Shoot bullets
            bulletTimeCounter += deltaTime;
            if (_isShooting && bulletTimeCounter >= bulletTimeLimiter) {
                bulletTimeCounter = 0;
                Shoot();
            }

            if (_stateMachine != null)
                _stateMachine.Update(deltaTime);
        }

        public override void OnDestroy() {
            _keyboard.KeyDownEvent -= KeyDownEventHandler;
            _keyboard.KeyUpEvent -= KeyUpEventHandler;
        }
        #endregion

        private const float turnRadians = 5.0f; 
        private Vector2 force;
        private void UpdatePhysics() {
            float currentRotation = Entity.GetComponent<TransformComponent>().CurrentRotation;
            force = (new Vector2(1.0f * (float)Math.Cos(currentRotation), 1.0f * (float)Math.Sin(currentRotation)));

            if (_isForceOn) {
                Physics.ApplyForce(force.X, force.Y);
                engineParticle.GetComponent<ParticleSystemComponent>().EmittorActive = true;
            }
            else
                engineParticle.GetComponent<ParticleSystemComponent>().EmittorActive = false;

            if (_isTurningLeft) {
                Physics.AngularVelocity = turnRadians;
            }
            else if (_isTurningRight) {
                Physics.AngularVelocity = -turnRadians;
            }

            StayOnScreen();
        }

        private bool _isForceOn;
        private bool _isTurningRight;
        private bool _isTurningLeft;
        private bool _isShooting;
        private void KeyDownEventHandler(object sender, KeyEventArgs e) {
            // Force
            if (e.KeyCode == Keys.W) {
                _isForceOn = true;
                //Console.WriteLine("Moving forward");
            }

            // Turn Left
            else if (e.KeyCode == Keys.A) {
                _isTurningLeft = true;
                //Console.WriteLine("Turning Left");
            }

            // Turn Right
            else if (e.KeyCode == Keys.D) {
                _isTurningRight = true;
                //Console.WriteLine("Turning Right");
            }

            // Shoot bullets
            else if (e.KeyCode == Keys.Space) {
                _isShooting = true;
                //Console.WriteLine("Shooting bullets");
            }
        }

        private void KeyUpEventHandler(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.W)
                _isForceOn = false;

            if (e.KeyCode == Keys.A)
                _isTurningLeft = false;
            if (e.KeyCode == Keys.D)
                _isTurningRight = false;

            if (_isTurningLeft == false && _isTurningRight == false) {
                Physics.AngularVelocity = 0;
            }

            if (e.KeyCode == Keys.Space)
                _isShooting = false;
        }

        private void StayOnScreen() {
            Vector2 shipPosition = Entity.GetComponent<PhysicsComponent>().CurrentPositionPixels();

            if (shipPosition.X > (gameScreenWidth / 2 + (shipWidth / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)-Game.GraphicsDevice.ResolutionX / 2 - (shipWidth / 2), shipPosition.Y));
            else if (shipPosition.X < (-gameScreenWidth / 2 - (shipWidth / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2((float)Game.GraphicsDevice.ResolutionX / 2 + (shipWidth / 2), shipPosition.Y));

            if (shipPosition.Y > (gameScreenHeight / 2 + (shipHeight / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2(shipPosition.X, (float)-Game.GraphicsDevice.ResolutionY / 2 - (shipHeight / 2)));
            else if (shipPosition.Y < (-gameScreenHeight / 2 - (shipHeight / 2)))
                Entity.GetComponent<PhysicsComponent>().SetPosition(new Vector2(shipPosition.X, (float)Game.GraphicsDevice.ResolutionY / 2 + (shipHeight / 2)));
        }
    }
}

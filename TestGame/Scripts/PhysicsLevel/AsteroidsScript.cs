﻿using System;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.PhysicsSystem;
using SimpleGamesLibrary.ResourceSystem;

namespace TestGame.Scripts.PhysicsLevel {
    public class AsteroidsScript : ScriptComponent {
        private const int maxAsteroids = 5;
        private Entity[] asteroids;

        int gameScreenWidth;
        int gameScreenHeight;
        Random random;

        IPhysicsManagerService physicsManager;

        public override void Awaken() {
            physicsManager = Game.GameServices.GetService<IPhysicsManagerService>();
            asteroids = new Entity[maxAsteroids];

            gameScreenWidth = Game.GraphicsDevice.ResolutionX;
            gameScreenHeight = Game.GraphicsDevice.ResolutionY;

            random = new Random();
        }

        public override void Start() {
            for (int x = 0; x < maxAsteroids; x++) {
                Entity asteroid = EntityManager.CreateEntity("asteroid" + AsteroidScript.asteroidCounter, "asteroids");
                var transform = asteroid.AddComponent<TransformComponent>();
                var sprite = asteroid.AddComponent<SpriteComponent>();
                var physics = asteroid.AddComponent<PhysicsComponent>();
                var script = asteroid.AddComponent<AsteroidScript>();

                transform.SetRotation(MathHelper.ToRadians(random.Next(0, 360)));
                transform.SetPosition(new Vector2(random.Next(-300, 300), random.Next(-300, 300)));

                script.CurrentState = AsteroidScript.State.LargeAsteroid;
                sprite.Texture = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/AsteroidL.png", Game.GraphicsDevice.Device));

                physics.CreateRectangleBody(sprite.Texture.Info.Width, sprite.Texture.Info.Height, 10);
                physics.SetCollisionCategory(Category.Cat2); // asteroids
                physics.SetCollidesWith(Category.Cat1 | Category.Cat4); //spaceship and bullets

                AsteroidScript.asteroidCounter++;
            }
        }

        public override void Update(float deltaTime) {
         
        }

        public override void OnDestroy() {
          
        }
    }
}

﻿using System.Windows.Forms;
using FarseerPhysics.Xna;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.InputSystem;

namespace TestGame.Scripts {
    public class CameraScript : ScriptComponent {

        private IMouse _mouse;

        public CameraScript() : base() {}

        public override void Awaken() {
            if (Transform == null)
                Enabled = false;
            
            _mouse = Game.GameServices.GetService<IInputManagerService>().GetMouse();
            //_mouse.MouseMoveEvent += MouseMoveEventHandler;
            
            
            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
        }

        public override void Start() {

        }

        public override void OnDestroy() {
            Entity.AddComponentEvent -= ComponentAddedToEntityHandler;
        }

        public override void Update(float deltaTime) {

        }

        private void MouseMoveEventHandler(object sender, MouseEventArgs e) {
            Transform.SetPosition(new Vector2(e.X - (Game.Form.ClientRectangle.Width / 2), -e.Y + (Game.Form.ClientRectangle.Height / 2)));
        }

        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {
            if (attachEventArgs.Component is CameraComponent) {
                Camera = attachEventArgs.Component as CameraComponent;
                Enabled = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.ResourceSystem;

namespace TestGame.Scripts.MenuLevel {
    public class MenuLevelGuiScript : ScriptComponent {
        private int _centerScreenX;
        private int _centerScreenY;
        private const int _buttonOffsetX = 107;

        private ButtonDescriptor _gameplayLevel;
        private ButtonDescriptor _physicsLevel;
        private ButtonDescriptor _quit;

        public override void Awaken() {
            GuiManager.GUIEvent += GuiEventHandler;

            _centerScreenX = Game.GraphicsDevice.ResolutionX / 2;
            _centerScreenY = Game.GraphicsDevice.ResolutionY / 2;

            InitializeGuiElements();
        }

        private void InitializeGuiElements() {
            // GUI skin
            GUISkin mySkin = new GUISkin();
            mySkin.ButtonFont = Game.FontResourceManager.Load(new FontResourceDescriptor("Content/Fonts/Lucida_Calligraphy_Italic.ttf", Game.GraphicsDevice, 25));
            mySkin.ButtonDownImage = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/downbuttonimg.png", Game.GraphicsDevice.Device));
            mySkin.ButtonOverImage = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/overbuttonimg.png", Game.GraphicsDevice.Device));
            mySkin.ButtonUpImage = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/upbuttonimg.png", Game.GraphicsDevice.Device));
            mySkin.SliderButton = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/sliderbutton.png", Game.GraphicsDevice.Device));
            mySkin.SliderTrack = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/slidertrack.png", Game.GraphicsDevice.Device));
            mySkin.LabelFont = Game.FontResourceManager.Load(new FontResourceDescriptor("Content/Fonts/SCRIPTBL.ttf", Game.GraphicsDevice, 26));
            mySkin.CheckBoxChecked = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/checkboxchecked.png", Game.GraphicsDevice.Device));
            mySkin.CheckBoxEmpty = Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/checkboxempty.png", Game.GraphicsDevice.Device));
            GuiManager.Skin = mySkin;

            _gameplayLevel = new ButtonDescriptor("gameplayLevelButton", _centerScreenX - _buttonOffsetX, _centerScreenY + 10, 214, 64, "Gameplay Testing");
            _physicsLevel = new ButtonDescriptor("physicsLevelButton", _centerScreenX - _buttonOffsetX, _centerScreenY + 80, 214, 64, "Physics Testing");
            _quit = new ButtonDescriptor("quitbutton", _centerScreenX - _buttonOffsetX, _centerScreenY + 150, 214, 64, "Quit");
        }

        public override void OnDestroy() {
            GuiManager.GUIEvent -= GuiEventHandler;
        }

        public override void Update(float deltaTime) {
            
        }

        private void GuiEventHandler() {
            if (GuiManager.Button(_gameplayLevel)) {
                LevelManager.LoadLevel("gameplayLevel");
            }

            if (GuiManager.Button(_physicsLevel)) {
                LevelManager.LoadLevel("physicsLevel");
            }

            if (GuiManager.Button(_quit)) {
                Game.Quit();
            }
        }

        public override void Start() {

        }
    }
}

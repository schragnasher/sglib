﻿using SimpleGamesLibrary.EntitySystem;
using System.Drawing;
using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.AudioSystem;

namespace TestGame.Scripts {
    public class GameplayGuiScript : ScriptComponent {
        private int _centerScreenX;
        private int _centerScreenY;
        private const int _buttonOffsetX = 107;

        private ButtonDescriptor _newGameButton;
        private ButtonDescriptor _optionsButton;
        private ButtonDescriptor _backButton;

        private SoundResource _soundResource;
        private SoundResource _soundResource2;

        public override void Awaken() {
            GuiManager.GUIEvent += GuiEventHandler;

            _centerScreenX = Game.GraphicsDevice.ResolutionX / 2;
            _centerScreenY = Game.GraphicsDevice.ResolutionY / 2;

            _newGameButton = new ButtonDescriptor("newGameButton", _centerScreenX - _buttonOffsetX, _centerScreenY + 10, 214, 64, "New Game");
            _optionsButton = new ButtonDescriptor("optionsButton", _centerScreenX - _buttonOffsetX, _centerScreenY + 80, 214, 64, "Options");
            _backButton = new ButtonDescriptor("backButton", 0, 0, 214, 64, "Back");

             _soundResource = Game.SoundResourceManager.Load(new SoundResourceDescriptor("Content/Sounds/MenuMusic.wav", Game.GameServices.GetService<IAudioService>()));
             _soundResource2 = Game.SoundResourceManager.Load(new SoundResourceDescriptor("Content/Sounds/JewelClick.wav", Game.GameServices.GetService<IAudioService>()));
        }

        public override void OnDestroy() {
            GuiManager.GUIEvent -= GuiEventHandler;
        }

        public override void Update(float deltaTime) {

        }

        private void GuiEventHandler() {
            if (GuiManager.Button(_backButton)) {
                LevelManager.LoadLevel("menuLevel");
            }

            if (GuiManager.Button("soundButton", new Rectangle(0, 50, 214, 64), "Play Looping Music 1")) {
                AudioManager.PlayMusic(_soundResource, true);
            }

            if (GuiManager.Button("sound2Button", new Rectangle(0, 100, 214, 64), "Play Sound 1")) {
                AudioManager.PlaySound(_soundResource2, false);
            }

            if (GuiManager.Button("stopSoundsButton", new Rectangle(0, 150, 214, 64), "Stop All Sounds")) {
                AudioManager.StopAllSounds();
            }


           // TestScriptComponent._timeToTurn = GuiManager.Slider("slider1", new Rectangle(10, 400, 200, 30), TestScriptComponent._timeToTurn, 0.0f, 100.0f);
            //GuiManager.Label("slidervaluelabel", new Rectangle(250, 400, 50, 30), TestScriptComponent._timeToTurn.ToString());

            AudioManager.SoundVolume = GuiManager.Slider("soundVolumeSlider", new Rectangle(10, 430, 200, 30), AudioManager.SoundVolume, 0.0f, 100.0f);
            GuiManager.Label("soundVolumeLabel", new Rectangle(250, 440, 50, 30), "Sound");

            AudioManager.MusicVolume = GuiManager.Slider("musicVolumeSlider", new Rectangle(10, 500, 200, 30), AudioManager.MusicVolume, 0.0f, 100.0f);
            GuiManager.Label("musicVolumeLabel", new Rectangle(250, 510, 50, 30), "Music");

            AudioManager.MasterVolume = GuiManager.Slider("masterVolumeSlider", new Rectangle(10, 550, 200, 30), AudioManager.MasterVolume, 0.0f, 100.0f);
            GuiManager.Label("masterVolumeLabel", new Rectangle(250, 560, 50, 30), "Master");

            GuiManager.Image("image1", new Rectangle(300, 300, 100, 100), Game.TextureResourceManager.Load(new TextureResourceDescriptor("Content/Textures/upbuttonimg.png", Game.GraphicsDevice.Device)));

        }

        public override void Start() {

        }
    }
}


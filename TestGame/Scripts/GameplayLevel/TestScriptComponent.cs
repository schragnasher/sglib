﻿using FarseerPhysics.Xna;
using SimpleGamesLibrary.EntitySystem;

namespace TestGame.Scripts {
    public class TestScriptComponent : ScriptComponent {
        public static float _timeToTurn = 5.0f;
        private float _timePassed;
        private Vector2 _translateEast = new Vector2(1, 0);
        private Vector2 _translateWest = new Vector2(-1, 0);
        private Vector2 _direction;

        public override void Awaken() {
            if (Transform == null)
                Enabled = false;

            _direction = _translateEast;
            //Transform.SetScale(new Vector2(0.1f, 0.1f));
            Entity.AddComponentEvent += ComponentAddedToEntityHandler;
        }

        public override void OnDestroy() {
            Entity.AddComponentEvent -= ComponentAddedToEntityHandler;
        }

        public override void Update(float deltaTime) {
            _timePassed += deltaTime;

            TransformComponent _transformComponent = Entity.GetComponent<TransformComponent>();
            if (_timePassed >= _timeToTurn) {
                _timePassed = 0;
                if (_direction == _translateEast)
                    _direction = _translateWest;
                else
                    _direction = _translateEast;
            }

            _transformComponent.Translate(_direction);
            _transformComponent.Rotate(-0.01f);
            
            //_transformComponent.Scaling(new Vector2(0.99f,0.99f));

            //Transform.Scaling(new Vector2(1.01f,1.01f));
        }

        public void ComponentAddedToEntityHandler(object sender, AttachEventArgs attachEventArgs) {

            if (attachEventArgs.Component is TransformComponent) {
                Transform = attachEventArgs.Component as TransformComponent;
                Transform.SetScale(new Vector2(0.1f, 0.1f));
            }
        }

        public void ComponentRemovedFromEntityHandler(object sender, RemoveEventArgs eventArgs) {

        }

        public override void Start() {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.SceneSystem.ParticleSystem {
    public class ParticleEmitter {
        private TextureResource _particleTexture;
        private List<Particle> _particles;

        private struct Particle {
            public Vector2 Position { get; set; }
            public float Rotation { get; set; }
            public float Scale { get; set; }
        }

        public void Update() {
  
        }

        public void Render() {

        }
    }
}

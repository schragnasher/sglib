﻿using SlimDX.Direct3D9;
using SimpleGamesLibrary.ResourceSystem;
using SlimDX;
using System.Drawing;
using SimpleGamesLibrary.Utilities;

namespace SimpleGamesLibrary.SceneSystem {
    
    public class SpriteNode : Node {
        private Vector2 _origin;
        private TextureResource _texture;

        public TextureResource Texture {
            get { return _texture; }
            set { 
                _texture = value;
                SubTexture = new Rectangle(0, 0, value.Info.Width, value.Info.Height);
                _origin = new Vector2((float)value.Info.Width / 2, (float)value.Info.Height / 2);
            } 
        }

        public FarseerPhysics.Xna.Vector2 Origin {
            get { return Conversion.ToFarseerVector2(_origin); }
            set { _origin = Conversion.ToVector2(value); }
        }
        public Rectangle SubTexture { get; set; }

        public Color4 Color { get; set; }

        public SpriteNode() {
            SubTexture = Rectangle.Empty;
            Color = new Color4(1, 1, 1, 1); // white
            _origin = Vector2.Zero;
        }

        public override void Update(float deltaTime) {

        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {
            if (Texture != null) {
                ColoredTexturedVertex[] vertices = {
                    new ColoredTexturedVertex(
                        new Vector3(-Origin.X + SubTexture.X, Origin.Y - SubTexture.Y,0), Color, 
                        (float)SubTexture.X / (float)Texture.Info.Width, (float)SubTexture.Y / (float)Texture.Info.Height),
                    new ColoredTexturedVertex(
                        new Vector3(-Origin.X + SubTexture.Width + SubTexture.X, Origin.Y - SubTexture.Y,0), Color,
                        ((float)SubTexture.X + (float)SubTexture.Width) / (float)Texture.Info.Width, (float)SubTexture.Y / (float)Texture.Info.Height),
                    new ColoredTexturedVertex(
                        new Vector3(-Origin.X + SubTexture.X, Origin.Y - SubTexture.Y - SubTexture.Height,0), Color,
                        (float)SubTexture.X / (float)Texture.Info.Width, ((float)SubTexture.Y + (float)SubTexture.Height) / (float)Texture.Info.Height),
                    new ColoredTexturedVertex(
                        new Vector3(-Origin.X + SubTexture.Width + SubTexture.X, Origin.Y - SubTexture.Y - SubTexture.Height,0), Color,
                        ((float)SubTexture.X + (float)SubTexture.Width) / (float)Texture.Info.Width, ((float)SubTexture.Y + (float)SubTexture.Height) / (float)Texture.Info.Height)
                };

                device.VertexFormat = ColoredTexturedVertex.Format;
                device.SetTexture(0, _texture.Texture);
                device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
                device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
                device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);
            }
        }

        public override void Destroy() {
            if (Parent != null) {
                Parent.Remove(this);
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.SceneSystem {

    public class CameraNode : Node {

        public CameraNode() {

        }

        public override void Update(float deltaTime) {

        }

        public void SetTransform(Device device) { 
            Matrix transform = Matrix.Identity;
            Node n = this;
            while(n!=null) { 
                if(n is TransformNode){
                    transform *= ((TransformNode)n).Transform;
                }
                n = n.Parent;
            }
            device.SetTransform(TransformState.View, Matrix.Invert(transform));
        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {

        }

        public override void Destroy() {
            if (Parent != null) {
                Parent.Remove(this);
            }
        }
        
    }

}

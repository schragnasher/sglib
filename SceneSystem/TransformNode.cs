﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.Utilities;
using SlimDX;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.SceneSystem {
    
    public class TransformNode : GroupNode {

        private Vector2 _prevPosition;
        private Vector2 _nextPosition;
        private float _prevRotation;
        private float _nextRotation;
        private Vector2 _prevScale;
        private Vector2 _nextScale;

        public FarseerPhysics.Xna.Vector2 Position { get { return Conversion.ToFarseerVector2(_nextPosition); } }
        public float Rotation { get { return _nextRotation; } }
        public FarseerPhysics.Xna.Vector2 Scale { get { return Conversion.ToFarseerVector2(_nextScale); } }

        //Note: This seems to only be used at the node level, so not converting to Farseer vector
        public Matrix Transform {
            get {
                return 
                    Matrix.RotationZ((_nextRotation)) *
                    Matrix.Scaling(new Vector3(_nextScale, 1.0f)) *
                    Matrix.Translation(new Vector3(_nextPosition, 0));
            } 
        }

        public TransformNode() {
            _prevPosition = Vector2.Zero;
            _nextPosition = Vector2.Zero;
            _prevRotation = 0.0f;
            _nextRotation = 0.0f;
            _prevScale = new Vector2(1.0f, 1.0f);
            _nextScale = new Vector2(1.0f, 1.0f);
        }

        public void SetPosition(FarseerPhysics.Xna.Vector2 position) {
            _nextPosition = Conversion.ToVector2(position);
            _prevPosition = Conversion.ToVector2(position);
        }

        public void SetRotation(float radians) {
            _nextRotation = radians;
            _prevRotation = radians;
        }

        public void SetScale(FarseerPhysics.Xna.Vector2 scale) {
            _nextScale = Conversion.ToVector2(scale);
            _prevScale = Conversion.ToVector2(scale);
        }

        public void Translate(FarseerPhysics.Xna.Vector2 position) {
            _nextPosition += Conversion.ToVector2(position);
        }

        public void Rotate(float degrees) {
            _nextRotation += degrees;
        }

        public void Scaling(FarseerPhysics.Xna.Vector2 scale) {
            _nextScale += Conversion.ToVector2(scale);
        }

        public override void Update(float deltaTime) {
            _prevPosition = _nextPosition;
            _prevRotation = _nextRotation;
            _prevScale = _nextScale;
            base.Update(deltaTime);
        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {
            Matrix nextTransform =
                Matrix.RotationZ((_nextRotation)) *
                Matrix.Scaling(new Vector3(_nextScale, 1.0f)) *
                Matrix.Translation(new Vector3(_nextPosition, 0));
            Matrix prevTransform =
                Matrix.RotationZ((_prevRotation)) *
                Matrix.Scaling(new Vector3(_prevScale, 1.0f)) *
                Matrix.Translation(new Vector3(_prevPosition, 0));
            matrixStack.Push();
            matrixStack.MultiplyMatrix(Matrix.Lerp(prevTransform, nextTransform, alpha));
            device.SetTransform(TransformState.World, matrixStack.Top);
            base.Render(device, matrixStack, alpha);
            matrixStack.Pop();
            device.SetTransform(TransformState.World, matrixStack.Top);
        }

        public override void Destroy() {
            if (Parent != null) {
                Parent.Remove(this);
            }
        }

    }

}

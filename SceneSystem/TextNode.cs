﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleGamesLibrary.ResourceSystem;
using SlimDX;
using SlimDX.Direct3D9;
using System.Drawing;

namespace SimpleGamesLibrary.SceneSystem {
    public class TextNode : Node {

        public FontResource Font { get; set; }
        public string Text { get; set; }
        public Color4 Color { get; set; }
        private Rectangle Rectangle { get; set; }
        public DrawTextFormat Format { get; set; }

        public TextNode() {
            Text = "";
            Color = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            Rectangle = Rectangle.Empty;
            Format = DrawTextFormat.Center | DrawTextFormat.VerticalCenter;
        }

        public override void Update(float deltaTime) {
            // Do Nothing
        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {
            if (Font != null) {
                Rectangle measurement = Font.Font.MeasureString(null, Text, Format);
                Vector3 pos = Vector3.Project(new Vector3(measurement.X, measurement.Y*-1.0f, 0), 0, 0, device.Viewport.Width, device.Viewport.Height, 0, 1, device.GetTransform(TransformState.World) * device.GetTransform(TransformState.View) * device.GetTransform(TransformState.Projection));
                Rectangle pRectangle = new Rectangle((int)pos.X, (int)pos.Y + measurement.Y, measurement.Width, measurement.Height);
                Font.Font.DrawString(null, Text, pRectangle, Format, Color);
            }
        }

        public override void Destroy() {
            if (Parent != null) {
                Parent.Remove(this);
            }
        }
    }
}

﻿using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.SceneSystem {

    public abstract class Node {

        public GroupNode Parent { get; set; }

        public Node() { }
        public abstract void Update(float deltaTime);
        public abstract void Render(Device device, MatrixStack matrixStack, float alpha);
        public abstract void Destroy();

    }

}

﻿using System;
using System.Collections.Generic;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.Utilities;
using SlimDX;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.SceneSystem {
    public class ParticleSystemNode : Node {
        private class Particle {
            public Vector2 Position;
            public float Rotation;
            public Vector2 Size;
            public Vector2 Velocity;
            public Color4 Color;
            public float Age;
            public float Lifetime;
        }

        #region Fields
        private Random _randomGenerator = new Random();

        private List<Particle> _particles = new List<Particle>();

        private Vector2 _origin;
        private TextureResource _texture;
        private Vector2 _offset;
        private Vector2 _startVelocity;
        private Vector2 _randVelocity;

        #endregion

        #region Properties
        public bool EmittorActive { get; set; }
        public bool OneShot { get; set; }
        public bool EmittorDone {
            get { return _particles.Count == 0; }
        }

        public FarseerPhysics.Xna.Vector2 Offset {
            get { return Conversion.ToFarseerVector2(_offset); }
            set { _offset = Conversion.ToVector2(value); }

        }
        public FarseerPhysics.Xna.Vector2 StartVelocity {
            get { return Conversion.ToFarseerVector2(_startVelocity); }
            set { _startVelocity = Conversion.ToVector2(value); }
        }
        public FarseerPhysics.Xna.Vector2 RandVelocity {
            get { return Conversion.ToFarseerVector2(_randVelocity); }
            set { _randVelocity = Conversion.ToVector2(value); }
        }
        public float MinStartingSize { get; set; }
        public float MaxStartingSize { get; set; }
        public float MinLifetime { get; set; }
        public float MaxLifetime { get; set; }
        public float MinEmission { get; set; }
        public float MaxEmission { get; set; }
        public float TimeToNextEmission { get; set; }
        public Color4 StartColor { get; set; }
        public Color4 EndColor { get; set; }

        public FarseerPhysics.Xna.Vector2 Origin {
            get { return Conversion.ToFarseerVector2(_origin); }
            set { _origin = Conversion.ToVector2(value); }
        }

        public TextureResource Texture {
            get { return _texture; }
            set {
                _texture = value;
                _origin = new Vector2((float)value.Info.Width / 2, (float)value.Info.Height / 2);
            }
        }
        #endregion

        public ParticleSystemNode() {
            EmittorActive = false;
            OneShot = false;
            _offset = new Vector2(0, 0);
            _startVelocity = new Vector2(0, 0);
            _randVelocity = new Vector2(0, 0);
            MinStartingSize = 1.0f;
            MaxStartingSize = 1.0f;
            MinLifetime = 100.0f;
            MaxLifetime = 100.0f;
            MinEmission = 100.0f;
            MaxEmission = 100.0f;
            TimeToNextEmission = 0.0f;
            StartColor = new Color4(0, 0, 0, 0);
            EndColor = new Color4(0, 0, 0, 0);

            _particles.Add(new Particle());
        }

        private void Emit() {
            Particle particle = new Particle();

            particle.Position = new Vector2(0, 0);
            particle.Rotation = 0;

            float randomSize = (float)((MaxStartingSize - MinStartingSize) * _randomGenerator.NextDouble() + MinStartingSize);
            particle.Size = new Vector2(randomSize, randomSize);

            particle.Velocity = _startVelocity + new Vector2((_randVelocity.X - (_randVelocity.X * -1.0f)) * (float)_randomGenerator.NextDouble() + (_randVelocity.X * -1.0f),
                (_randVelocity.Y - (_randVelocity.Y * -1.0f)) * (float)_randomGenerator.NextDouble() + (_randVelocity.Y * -1.0f));

            particle.Color = StartColor;
            particle.Age = 0.0f;
            particle.Lifetime = (float)((MaxLifetime - MinLifetime) * _randomGenerator.NextDouble() + MinLifetime);

            _particles.Add(particle);

            TimeToNextEmission = (float)(((1 / MaxEmission) - (1 / MinEmission)) * _randomGenerator.NextDouble() + (1 / MinEmission));
        }

        #region Node Members
        public override void Update(float deltaTime) {
            if (EmittorActive && TimeToNextEmission <= 0) {
                if (OneShot) {
                    for (int particleToEmit = 0; particleToEmit < MaxEmission; particleToEmit++) {
                        Emit();
                        EmittorActive = false;
                    }
                }
                else
                    Emit();
            }

            foreach (Particle particle in _particles) {
                particle.Age += deltaTime;
                particle.Color = Color4.Lerp(StartColor, EndColor, particle.Age / particle.Lifetime);
                particle.Position += particle.Velocity;
            }

            for (int x = _particles.Count - 1; x >= 0; x--) {
                if (_particles[x].Age > _particles[x].Lifetime) { _particles.Remove(_particles[x]); }
            }

            TimeToNextEmission -= deltaTime;
        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {
            foreach (Particle particle in _particles) {
                device.SetTransform(TransformState.World, 
                    Matrix.Scaling(particle.Size.X, particle.Size.Y, 0) * 
                    Matrix.RotationZ(particle.Rotation) * 
                    Matrix.Translation(particle.Position.X, particle.Position.Y, 0) *
                    matrixStack.Top);
 
                if (Texture != null) {
                    ColoredTexturedVertex[] vertices = {
                    new ColoredTexturedVertex(new Vector3(-Origin.X, Origin.Y, 0), new Color4(1,1,1,1), 0, 0),
                    new ColoredTexturedVertex(new Vector3(-Origin.X + Texture.Info.Width, Origin.Y, 0), particle.Color, 1, 0),
                    new ColoredTexturedVertex(new Vector3(-Origin.X, Origin.Y - Texture.Info.Height, 0), particle.Color, 0, 1),
                    new ColoredTexturedVertex(new Vector3(-Origin.X + Texture.Info.Width, Origin.Y - Texture.Info.Height, 0), particle.Color, 1, 1)
                };

                    device.VertexFormat = ColoredTexturedVertex.Format;
                    device.SetTexture(0, _texture.Texture);
                    device.DrawUserPrimitives<ColoredTexturedVertex>(PrimitiveType.TriangleStrip, 2, vertices);
                }
            }


            device.SetTransform(TransformState.World, matrixStack.Top);
        }

        public override void Destroy() {
            if (Parent != null) {
                Parent.Remove(this);
            }
        }

        #endregion
    }
}

﻿#region Using Ststements
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX;
using SlimDX.Direct3D9;
#endregion

namespace SimpleGamesLibrary.SceneSystem {

    public class SceneManager : ISceneManagerService, IDisposable {

        public readonly int MaxLayers = 8;
        public CameraNode ActiveCamera { get; set; }

        private IGame _game;
        private GraphicsDevice _graphicsDevice;
        private GroupNode _root = new GroupNode();
        private MatrixStack _matrixStack = new MatrixStack();

        #region Constructor
        public SceneManager(IGame game) {
            _game = game;
            _game.GameServices.RegisterService<ISceneManagerService>(this);
            _graphicsDevice = _game.GraphicsDevice;
            for (int x = 0; x < 8; x++) { _root.Add(new GroupNode()); }
        }
        #endregion

        public void Initialize() {

        }

        public void AddNode(Node node, int layer) {
            if (layer > MaxLayers - 1) { layer = MaxLayers; }
            if (layer < 0) { layer = 0; }
            (_root.children[layer] as GroupNode).Add(node);
        }

        #region Update/Render
        public void Update(float deltaTime) {
            _root.Update(deltaTime);
        }

        public void Render(float alpha) {
            if (ActiveCamera != null) {
                ActiveCamera.SetTransform(_graphicsDevice.Device);
                _root.Render(_game.GraphicsDevice.Device, _matrixStack, alpha);
            }
        }
        #endregion

        #region Dispose
        public void Dispose() {
            _matrixStack.Dispose();
        }
        #endregion

    }

}

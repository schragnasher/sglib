﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.SceneSystem {

    public class GroupNode : Node {

        public List<Node> children = new List<Node>();

        public GroupNode() {

        }

        public void Add(Node node) {
            node.Parent = this;
            children.Add(node);
        }

        public void Remove(Node node) {
            children.Remove(node);
        }

        public override void Update(float deltaTime) {
            UpdateChildren(deltaTime);
        }

        public override void Render(Device device, MatrixStack matrixStack, float alpha) {
            RenderChildren(device, matrixStack, alpha);
        }

        private void UpdateChildren(float deltaTime) {
            foreach (Node n in children) {
                n.Update(deltaTime);
            }
        }

        private void RenderChildren(Device device, MatrixStack matrixStack, float alpha) {
            foreach (Node n in children) {
                n.Render(device, matrixStack, alpha);
            }
        }

        public override void Destroy() {
            foreach (Node child in children) {
                child.Destroy();
            }
            if (Parent != null) {
                Parent.Remove(this);
            }
        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.SceneSystem {

    public interface ISceneManagerService {

        CameraNode ActiveCamera { get; set; }

        void AddNode(Node node, int layer);

    }

}

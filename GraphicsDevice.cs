﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SlimDX;
using SlimDX.Direct3D9;
using System.Drawing.Text;

namespace SimpleGamesLibrary {
    public class GraphicsDevice {

        public event Action DeviceLostEvent;
        public event Action DeviceResetEvent;

        public Device Device { get; private set; }
        public int ResolutionX { get; private set; }
        public int ResolutionY { get; private set; }
        public bool Fullscreen { get; private set; }

        private Form _form;
        private Direct3D direct3d;
        private PresentParameters windowedparameters;
        private PresentParameters fullscreenparameters;
        private PrivateFontCollection privatefontcollection = new PrivateFontCollection();
        private Surface _cursorSurface;
        private string _cursorFileName;
        private bool _deviceLost = false;
        private bool _modeSwitched = false;

        public GraphicsDevice(IGame game, int width, int height, bool fullscreen) {
            _form = game.Form;
            ResolutionX = width;
            ResolutionY = height;
            Fullscreen = fullscreen;
            if (Fullscreen) {
                _form.ClientSize = new Size(width, height);
                _form.TopMost = true;
                _form.FormBorderStyle = FormBorderStyle.None;
                _form.StartPosition = FormStartPosition.WindowsDefaultLocation;
            }
            else {
                _form.ClientSize = new Size(width, height);
                _form.FormBorderStyle = FormBorderStyle.Fixed3D;
                _form.MaximizeBox = false;
                _form.StartPosition = FormStartPosition.CenterScreen;
            }

            fullscreenparameters = new PresentParameters();
            fullscreenparameters.BackBufferWidth = _form.ClientSize.Width;
            fullscreenparameters.BackBufferHeight = _form.ClientSize.Height;
            fullscreenparameters.BackBufferFormat = Format.X8R8G8B8;
            fullscreenparameters.BackBufferCount = 1;
            fullscreenparameters.Multisample = MultisampleType.None;
            fullscreenparameters.MultisampleQuality = 0;
            fullscreenparameters.SwapEffect = SwapEffect.Discard;
            fullscreenparameters.DeviceWindowHandle = _form.Handle;
            fullscreenparameters.Windowed = false;
            fullscreenparameters.EnableAutoDepthStencil = false;
            fullscreenparameters.AutoDepthStencilFormat = 0;
            fullscreenparameters.PresentFlags = 0;
            fullscreenparameters.FullScreenRefreshRateInHertz = 59;
            fullscreenparameters.PresentationInterval = PresentInterval.Default;

            windowedparameters = new PresentParameters();
            windowedparameters.BackBufferWidth = _form.ClientSize.Width;
            windowedparameters.BackBufferHeight = _form.ClientSize.Height;
            windowedparameters.BackBufferFormat = Format.X8R8G8B8;
            windowedparameters.BackBufferCount = 1;
            windowedparameters.Multisample = MultisampleType.None;
            windowedparameters.MultisampleQuality = 0;
            windowedparameters.SwapEffect = SwapEffect.Discard;
            windowedparameters.DeviceWindowHandle = _form.Handle;
            windowedparameters.Windowed = true;
            windowedparameters.EnableAutoDepthStencil = false;
            windowedparameters.AutoDepthStencilFormat = 0;
            windowedparameters.PresentFlags = 0;
            windowedparameters.FullScreenRefreshRateInHertz = 0;
            windowedparameters.PresentationInterval = PresentInterval.Default;

            direct3d = new Direct3D();

            if (fullscreen) {
                Device = new Device(direct3d, 0, DeviceType.Hardware, _form.Handle, CreateFlags.HardwareVertexProcessing, fullscreenparameters);
            }
            else {
                Device = new Device(direct3d, 0, DeviceType.Hardware, _form.Handle, CreateFlags.HardwareVertexProcessing, windowedparameters);
            }

            Device.Viewport = new Viewport(0, 0, _form.ClientSize.Width, _form.ClientSize.Height);
            Device.SetTransform(TransformState.Projection, Matrix.Translation(-0.5f, 0.5f, 0.0f) * Matrix.OrthoLH(_form.ClientSize.Width, _form.ClientSize.Height, 0, 1));
            Device.SetTransform(TransformState.View, Matrix.Identity);
            Device.SetTransform(TransformState.World, Matrix.Identity);

            Device.SetRenderState(RenderState.AlphaBlendEnable, true);
            Device.SetRenderState(RenderState.AlphaFunc, Compare.Greater);
            Device.SetRenderState(RenderState.AlphaRef, 0x00);
            Device.SetRenderState(RenderState.AlphaTestEnable, true); // check this one
            Device.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            Device.SetRenderState(RenderState.Clipping, true);
            Device.SetRenderState(RenderState.ClipPlaneEnable, false);
            Device.SetRenderState(RenderState.ColorWriteEnable, ColorWriteEnable.Alpha | ColorWriteEnable.Blue | ColorWriteEnable.Green | ColorWriteEnable.Red);
            Device.SetRenderState(RenderState.CullMode, Cull.None);
            Device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            Device.SetRenderState(RenderState.DiffuseMaterialSource, ColorSource.Color1);
            Device.SetRenderState(RenderState.EnableAdaptiveTessellation, false);
            Device.SetRenderState(RenderState.FillMode, FillMode.Solid);
            Device.SetRenderState(RenderState.FogEnable, false);
            Device.SetRenderState(RenderState.IndexedVertexBlendEnable, false);
            Device.SetRenderState(RenderState.Lighting, false);
            Device.SetRenderState(RenderState.RangeFogEnable, false);
            Device.SetRenderState(RenderState.SeparateAlphaBlendEnable, false);
            Device.SetRenderState(RenderState.ShadeMode, ShadeMode.Gouraud);
            Device.SetRenderState(RenderState.SpecularEnable, false);
            Device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            Device.SetRenderState(RenderState.SrgbWriteEnable, false);
            Device.SetRenderState(RenderState.StencilEnable, false);
            Device.SetRenderState(RenderState.VertexBlend, false);
            Device.SetRenderState(RenderState.Wrap0, 0);
            Device.SetRenderState(RenderState.ZEnable, false);

            Device.SetTextureStageState(0, TextureStage.AlphaArg1, TextureArgument.Texture);
            Device.SetTextureStageState(0, TextureStage.AlphaArg2, TextureArgument.Diffuse);
            Device.SetTextureStageState(0, TextureStage.AlphaOperation, TextureOperation.Modulate);
            Device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
            Device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Diffuse);
            Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.Modulate);
            Device.SetTextureStageState(0, TextureStage.TexCoordIndex, 0);
            Device.SetTextureStageState(0, TextureStage.TextureTransformFlags, TextureTransform.Disable);
            Device.SetTextureStageState(1, TextureStage.AlphaOperation, TextureTransform.Disable);
            Device.SetTextureStageState(1, TextureStage.ColorOperation, TextureTransform.Disable);

            Device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            Device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            Device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            Device.SetSamplerState(0, SamplerState.MaxMipLevel, 0);
            //Device.SetSamplerState(0, SamplerState.MaxAnisotropy, direct3d.GetDeviceCaps(0, DeviceType.Hardware).MaxAnisotropy);
            Device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            Device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Point);
            Device.SetSamplerState(0, SamplerState.MipMapLodBias, 0);
            Device.SetSamplerState(0, SamplerState.SrgbTexture, 0);
        }
        
        public void Reset() {
            if (Fullscreen) {
                _form.ClientSize = new Size(ResolutionX, ResolutionY);
                _form.TopMost = true;
                _form.FormBorderStyle = FormBorderStyle.None;
                _form.StartPosition = FormStartPosition.WindowsDefaultLocation;
                Device.Reset(fullscreenparameters);
            }
            else {
                _form.ClientSize = new Size(ResolutionX, ResolutionY);
                _form.FormBorderStyle = FormBorderStyle.Fixed3D;
                _form.MaximizeBox = false;
                _form.StartPosition = FormStartPosition.WindowsDefaultLocation;
                _form.Location = new Point(10, 10);
                Device.Reset(windowedparameters);
            }

            Device.Viewport = new Viewport(0, 0, _form.ClientSize.Width, _form.ClientSize.Height);
            Device.SetTransform(TransformState.Projection, Matrix.Translation(-0.5f, 0.5f, 0.0f) * Matrix.OrthoLH(_form.ClientSize.Width, _form.ClientSize.Height, 0, 1));
            Device.SetTransform(TransformState.View, Matrix.Identity);
            Device.SetTransform(TransformState.World, Matrix.Identity);

            Device.SetRenderState(RenderState.AlphaBlendEnable, true);
            Device.SetRenderState(RenderState.AlphaFunc, Compare.Greater);
            Device.SetRenderState(RenderState.AlphaRef, 0x00);
            Device.SetRenderState(RenderState.AlphaTestEnable, true); // check this one
            Device.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            Device.SetRenderState(RenderState.Clipping, true);
            Device.SetRenderState(RenderState.ClipPlaneEnable, false);
            Device.SetRenderState(RenderState.ColorWriteEnable, ColorWriteEnable.Alpha | ColorWriteEnable.Blue | ColorWriteEnable.Green | ColorWriteEnable.Red);
            Device.SetRenderState(RenderState.CullMode, Cull.None);
            Device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            Device.SetRenderState(RenderState.DiffuseMaterialSource, ColorSource.Color1);
            Device.SetRenderState(RenderState.EnableAdaptiveTessellation, false);
            Device.SetRenderState(RenderState.FillMode, FillMode.Solid);
            Device.SetRenderState(RenderState.FogEnable, false);
            Device.SetRenderState(RenderState.IndexedVertexBlendEnable, false);
            Device.SetRenderState(RenderState.Lighting, false);
            Device.SetRenderState(RenderState.RangeFogEnable, false);
            Device.SetRenderState(RenderState.SeparateAlphaBlendEnable, false);
            Device.SetRenderState(RenderState.ShadeMode, ShadeMode.Gouraud);
            Device.SetRenderState(RenderState.SpecularEnable, false);
            Device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            Device.SetRenderState(RenderState.SrgbWriteEnable, false);
            Device.SetRenderState(RenderState.StencilEnable, false);
            Device.SetRenderState(RenderState.VertexBlend, false);
            Device.SetRenderState(RenderState.Wrap0, 0);
            Device.SetRenderState(RenderState.ZEnable, false);

            Device.SetTextureStageState(0, TextureStage.AlphaArg1, TextureArgument.Texture);
            Device.SetTextureStageState(0, TextureStage.AlphaArg2, TextureArgument.Diffuse);
            Device.SetTextureStageState(0, TextureStage.AlphaOperation, TextureOperation.Modulate);
            Device.SetTextureStageState(0, TextureStage.ColorArg1, TextureArgument.Texture);
            Device.SetTextureStageState(0, TextureStage.ColorArg2, TextureArgument.Diffuse);
            Device.SetTextureStageState(0, TextureStage.ColorOperation, TextureOperation.Modulate);
            Device.SetTextureStageState(0, TextureStage.TexCoordIndex, 0);
            Device.SetTextureStageState(0, TextureStage.TextureTransformFlags, TextureTransform.Disable);
            Device.SetTextureStageState(1, TextureStage.AlphaOperation, TextureTransform.Disable);
            Device.SetTextureStageState(1, TextureStage.ColorOperation, TextureTransform.Disable);

            Device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            Device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            Device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            Device.SetSamplerState(0, SamplerState.MaxMipLevel, 0);
            //Device.SetSamplerState(0, SamplerState.MaxAnisotropy, direct3d.GetDeviceCaps(0, DeviceType.Hardware).MaxAnisotropy);
            Device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            Device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Point);
            Device.SetSamplerState(0, SamplerState.MipMapLodBias, 0);
            Device.SetSamplerState(0, SamplerState.SrgbTexture, 0);
        }

        public void SwitchDisplayMode() {
            Fullscreen = !Fullscreen;
            _modeSwitched = true;
        }

        public void ClearBuffer(Color color) {
            try {
                Device.Clear(ClearFlags.Target, color, 1.0f, 0);
                Device.BeginScene();
            }
            catch (SlimDXException e) {
                if (e.ResultCode == ResultCode.DeviceLost) {
                    _deviceLost = true;
                }
            }
        }

        public void PresentBuffer() {
            try {
                Device.EndScene();
                Device.Present();
            }
            catch(SlimDXException e) {
                if (e.ResultCode == ResultCode.DeviceLost) {
                    _deviceLost = true;
                }
            }
        }

        private void OnDeviceLost() {
            if (DeviceLostEvent != null) { DeviceLostEvent(); }
        }

        private void OnDeviceReset() {
            if (DeviceResetEvent != null) { DeviceResetEvent(); }
        }

        public bool IsDeviceLost() {
            if (_modeSwitched) {
                OnDeviceLost();
                Reset();
                OnDeviceReset();
                //SetHardwareCursor(_cursorFileName);
                _modeSwitched = false;
            }
            Result result = Device.TestCooperativeLevel();
            if (result == ResultCode.DeviceLost || _deviceLost) {
                OnDeviceLost();
                _deviceLost = false;
                return true;
            }
            else if (result == ResultCode.DeviceNotReset) {
                Reset();
                OnDeviceReset();
                //SetHardwareCursor(_cursorFileName);
                _deviceLost = false;
                return false;
            }
            else {
                return false;
            } 
        }

        public void Dispose() {
            Device.Dispose();
            direct3d.Dispose();
        }
    }
}

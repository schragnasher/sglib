﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.LevelSystem {
    public interface ILevelManagerService {
        void RegisterLevel(string levelId, ILevel level);
        void LoadLevel(string levelId);
        void UnloadLevel();
    }
}

﻿using System;
using System.Collections.Generic;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.PhysicsSystem;

namespace SimpleGamesLibrary.LevelSystem {
    public class LevelManager : ILevelManagerService {
        private readonly IGame _game;
        private IEntityManagerService _entityManager;
        private IPhysicsManagerService _physicsManager;

        private readonly Dictionary<string, ILevel> levels = new Dictionary<string, ILevel>();

        public LevelManager(IGame game) {
            _game = game;
            _game.GameServices.RegisterService<ILevelManagerService>(this);
        }

        public void Initialize() {
            _entityManager = _game.GameServices.GetService<IEntityManagerService>();
            _physicsManager = _game.GameServices.GetService<IPhysicsManagerService>();
        }

        public void RegisterLevel(string levelId, ILevel level) {
            System.Diagnostics.Debug.Assert(!levels.ContainsKey(levelId), String.Format("Level id {0} already exists", levelId));

            levels.Add(levelId, level);
        }

        public void LoadLevel(string levelId) {
            System.Diagnostics.Debug.Assert(levels.ContainsKey(levelId), String.Format("Cannot load Level id {0}, doesn't exist ", levelId));

            UnloadLevel();

            levels[levelId].Initialize();
            levels[levelId].Load();
        }

        //TODO: Should a level unload itself? aka call this method instead of being done via LoadLevel. Be sure to avoid circular call!
        public void UnloadLevel() {
            _entityManager.ClearEntities();
            _physicsManager.World.Clear();
        }
    }
}

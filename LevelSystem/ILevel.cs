﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleGamesLibrary.LevelSystem {
    public interface ILevel {
        void Initialize();
        void Load();
        void Unload();
    }
}

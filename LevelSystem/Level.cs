﻿using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.SceneSystem;
using SimpleGamesLibrary.ResourceSystem;

namespace SimpleGamesLibrary.LevelSystem {
    public abstract class Level : ILevel {
        protected IGame Game;
        protected IGUIManagerService GuiManager;
        protected ISceneManagerService SceneManager;
        protected IEntityManagerService EntityManager;
        protected ResourceManager<FontResource, FontResourceDescriptor> FontResourceManager;
        protected ResourceManager<TextureResource, TextureResourceDescriptor> TextureResourceManager;
        protected ResourceManager<SoundResource, SoundResourceDescriptor> SoundResourceManager;
        protected GraphicsDevice GraphicsDevice;

        protected Level(IGame game){
            Game = game;

            FontResourceManager = Game.FontResourceManager;
            TextureResourceManager = Game.TextureResourceManager;
            SoundResourceManager = Game.SoundResourceManager;
            GraphicsDevice = Game.GraphicsDevice;
        }

        public void Initialize() {
            GuiManager = Game.GameServices.GetService<IGUIManagerService>();
            SceneManager = Game.GameServices.GetService<ISceneManagerService>();
            EntityManager = Game.GameServices.GetService<IEntityManagerService>();
        }

        public abstract void Load();
        public abstract void Unload();
    }
}

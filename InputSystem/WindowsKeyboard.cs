﻿using System.Windows.Forms;

namespace SimpleGamesLibrary.InputSystem {

    public class WindowsKeyboard : IKeyboard {

        public event KeyEventHandler KeyDownEvent;
        public event KeyEventHandler KeyUpEvent;
        public event KeyPressEventHandler KeyPressEvent;

        private Form _form;

        public WindowsKeyboard(Form form) {
            _form = form;
            _form.KeyDown += OnKeyDown;
            _form.KeyUp += OnKeyUp;
            _form.KeyPress += OnKeyPress;
        }

        private void OnKeyDown(object sender, KeyEventArgs e) { if (KeyDownEvent != null) { KeyDownEvent(this, e); } }
        private void OnKeyUp(object sender, KeyEventArgs e) { if (KeyUpEvent != null) { KeyUpEvent(this, e); } }
        private void OnKeyPress(object sender, KeyPressEventArgs e) { if (KeyPressEvent != null) { KeyPressEvent(this, e); } }

    }

}

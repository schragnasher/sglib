﻿using System;
using System.Windows.Forms;

namespace SimpleGamesLibrary.InputSystem {

    public class InputManager : IInputManagerService, IDisposable {

        private readonly IGame Game;
        private readonly Form Form;
        private readonly WindowsMouse _mouse;
        private readonly WindowsKeyboard _keyboard;

        public InputManager(IGame game) {
            Game = game;
            Form = Game.Form;
            Game.GameServices.RegisterService<IInputManagerService>(this);
            _mouse = new WindowsMouse(Form);
            _keyboard = new WindowsKeyboard(Form);
        }

        public void Initialize() { 
        
        }

        public void Update() { }

        public IMouse GetMouse() { return _mouse; }

        public IKeyboard GetKeyboard() { return _keyboard; }

        public void Dispose() { }

    }

}

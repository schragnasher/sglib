﻿using System.Windows.Forms;
using SimpleGamesLibrary.Utilities;

namespace SimpleGamesLibrary.InputSystem {
    public class WindowsMouse : IMouse {
        private Form _form;

        public event MouseEventHandler MouseButtonDownEvent;
        public event MouseEventHandler MouseButtonUpEvent;
        public event MouseEventHandler MouseMoveEvent;

        private Cursor _cursor;

        public WindowsMouse(Form form) {
            _form = form;
            _form.MouseDown += OnMouseDown;
            _form.MouseUp += OnMouseUp;
            _form.MouseMove += OnMouseMove;
        }

        private void OnMouseDown(object sender, MouseEventArgs e) { if (MouseButtonDownEvent != null) { MouseButtonDownEvent(this, e); } }
        private void OnMouseUp(object sender, MouseEventArgs e) { if (MouseButtonUpEvent != null) { MouseButtonUpEvent(this, e); } }
        private void OnMouseMove(object sender, MouseEventArgs e) { if (MouseMoveEvent != null) { MouseMoveEvent(this, e); } }

        public void SetCursor(string filename) {
            _form.Cursor = NativeMethods.LoadCustomCursor(filename);
        }

    }

}

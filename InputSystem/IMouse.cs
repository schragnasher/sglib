﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleGamesLibrary.InputSystem {
    public interface IMouse : IInputDevice {
        event MouseEventHandler MouseButtonDownEvent;
        event MouseEventHandler MouseButtonUpEvent;
        event MouseEventHandler MouseMoveEvent;

        void SetCursor(string filename);
    }
}

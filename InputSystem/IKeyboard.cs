﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleGamesLibrary.InputSystem {

    public interface IKeyboard : IInputDevice {
        event KeyEventHandler KeyDownEvent;
        event KeyEventHandler KeyUpEvent;
        event KeyPressEventHandler KeyPressEvent;
    }

}

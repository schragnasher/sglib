﻿using System;
using System.Windows.Forms;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices;

namespace SimpleGamesLibrary.Utilities {
    static class NativeMethods {

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr LoadCursorFromFile(string path);
        
        public static Cursor LoadCustomCursor(string path) {
            IntPtr hCurs = LoadCursorFromFile(path);
            if (hCurs == IntPtr.Zero) throw new Win32Exception();
            var curs = new Cursor(hCurs);
            // Note: force the cursor to own the handle so it gets released properly
            var fi = typeof(Cursor).GetField("ownHandle", BindingFlags.NonPublic | BindingFlags.Instance);
            fi.SetValue(curs, true);
            return curs;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using SlimDX;

namespace SimpleGamesLibrary.Utilities {
    public static class Conversion {
        #region Colors
        public static Color3 ToColor3(Color color) {
            return new Color3(color.R, color.G, color.B);
        }

        public static Color4 ToColor4(float red, float green, float blue) {
            return new Color4(1.0f, red, green, blue);
        }

        public static Color4 ToColor4(Color color) {
            return new Color4(1.0f, color.R, color.G, color.B);
        }

        public static Color4 ToColor4(Color3 color) {
            return new Color4(1.0f, color.Red, color.Green, color.Blue);
        }
        #endregion

        #region Vertices
        public static Vector3 ToVector3(FarseerPhysics.Xna.Vector2 vector) {
            return new Vector3(vector.X, vector.Y, 0);
        }

        public static FarseerPhysics.Xna.Vector2 ToFarseerVector2(SlimDX.Vector2 vector) {
            return new FarseerPhysics.Xna.Vector2(vector.X, vector.Y);
        }

        public static Vector2 ToVector2(FarseerPhysics.Xna.Vector2 vector) {
            return new Vector2(vector.X, vector.Y);
        }
        #endregion

        public static byte[] ImageToByte(Image img) {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}

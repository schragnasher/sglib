﻿using System;
using System.Runtime.InteropServices;
using SlimDX;
using SlimDX.Direct3D9;

namespace SimpleGamesLibrary.Utilities {

    struct ColoredVertex {

        public Vector3 Position;
        public int Color;

        public ColoredVertex(Vector3 position, Color4 color) {
            Position = position;
            Color = color.ToArgb();
        }

        public static int SizeBytes {
            get { return Marshal.SizeOf(typeof(ColoredVertex)); }
        }

        public static VertexFormat Format {
            get { return VertexFormat.Position | VertexFormat.Diffuse; }
        }

    }

    struct ColoredTexturedVertex {

        public Vector3 Position;
        public int Color;
        public float Tu, Tv;

        public ColoredTexturedVertex(Vector3 position, Color4 color, float tu, float tv) {
            Position = position;
            Color = color.ToArgb();
            Tu = tu;
            Tv = tv;
        }

        public static int SizeBytes {
            get { return Marshal.SizeOf(typeof(ColoredTexturedVertex)); }
        }

        public static VertexFormat Format {
            get { return VertexFormat.Position | VertexFormat.Diffuse | VertexFormat.Texture1; }
        }

    }
}

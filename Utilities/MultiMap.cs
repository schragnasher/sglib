﻿using System;
using System.Collections.Generic;

namespace SimpleGamesLibrary.Utilities {
    public class MultiMap<K,V> {
        private Dictionary<K, List<V>> _dictionary = new Dictionary<K, List<V>>();

        public void Add(K key, V value) {
            List<V> list;

            if (_dictionary.TryGetValue(key, out list)) {
                list.Add(value);
            }
            else {
                list = new List<V>();
                list.Add(value);
                _dictionary[key] = list;
            }
        }

        public IEnumerable<K> Keys {
            get { return _dictionary.Keys; }
        }

        //public Dictionary<K, List<V>>.ValueCollection Values {
        //    get { return _dictionary.Values; }
        //}

        public IEnumerable<List<V>> Values {
            get { return _dictionary.Values; }
        }

        public bool TryGetValue(K key, out List<V> list) {
            list = this[key];

            if (list != null)
                return true;
            else
                return false;
        }

        public List<V> this[K key] {
            get {
                List<V> list;

                if (this._dictionary.TryGetValue(key, out list)) {
                    return list;
                }
                else {
                    System.Diagnostics.Debug.Assert(false, String.Format("Cannot return values - Key {0} does not exist in the multimap.", key));
                    return null;
                }
            }
        }

        public bool ContainsKey(K key) {
            if (_dictionary.ContainsKey(key))
                return true;
            else
                return false;
        }

        public bool Remove(K key) {
            if (_dictionary.Remove(key))
                return true;
            else
                return false;
        }
    }
}

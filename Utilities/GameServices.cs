﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SimpleGamesLibrary.Utilities {
    public class GameServices {
        readonly Dictionary<Type, Object> servicesCollection = new Dictionary<Type, Object>();

        public void RegisterService<K>(K serviceProvider) {
            servicesCollection.Add(typeof(K), serviceProvider);
        }

        public K GetService<K>() {
            if (servicesCollection.ContainsKey(typeof(K)))
                return (K)servicesCollection[(typeof(K))];
            else
                return default(K);
        }

        public void RemoveService<K>() {
            Debug.Assert(servicesCollection.ContainsKey(typeof(K)), String.Format("Trying to remove service {0}, but it is not registered",typeof(K)));
                servicesCollection.Remove(typeof(K));
        }
    }
}

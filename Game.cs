﻿#region Using Statements
using System;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using SimpleGamesLibrary.EntitySystem.Blueprints;
using SimpleGamesLibrary.PhysicsSystem;
using SlimDX.Windows;
using SimpleGamesLibrary.InputSystem;
using SimpleGamesLibrary.AudioSystem;
using SimpleGamesLibrary.ResourceSystem;
using SimpleGamesLibrary.SceneSystem;
using SimpleGamesLibrary.EntitySystem;
using SimpleGamesLibrary.GUISystem;
using SimpleGamesLibrary.Utilities;
using SimpleGamesLibrary.LevelSystem;
#endregion

namespace SimpleGamesLibrary {

    public abstract class Game : IGame, IDisposable {

        public Form Form { get; private set; }
        public GameServices GameServices { get; private set; }
        public GraphicsDevice GraphicsDevice { get; private set; }
        public Settings Settings { get; private set; }
        public ResourceManager<TextureResource, TextureResourceDescriptor> TextureResourceManager { get; private set; }
        public ResourceManager<FontResource, FontResourceDescriptor> FontResourceManager { get; private set; }
        public ResourceManager<SoundResource, SoundResourceDescriptor> SoundResourceManager { get; private set; }

        protected EntityManager EntityManager { get; private set; }
        protected InputManager InputManager { get; private set; }
        protected AudioManager AudioManager { get; private set; }
        protected SceneManager SceneManager { get; private set; }
        protected GUIManager GUIManager { get; private set; }
        protected LevelManager LevelManager { get; private set; }
        protected PhysicsManager PhysicsManager { get; private set; }
        protected BlueprintManager BlueprintManager { get; private set; }

        protected Game() {
            GameServices = new GameServices();
            Settings = Settings.Load("Settings.xml");
            Form = new RenderForm(Settings.GameName);

            EntityManager = new EntityManager(this);
            InputManager = new InputManager(this);
            GUIManager = new GUIManager(this);
            AudioManager = new AudioManager(this);
            GraphicsDevice = new GraphicsDevice(this, Settings.ResolutionX, Settings.ResolutionY, Settings.Fullscreen);
            SceneManager = new SceneManager(this);
            TextureResourceManager = new ResourceManager<TextureResource, TextureResourceDescriptor>();
            FontResourceManager = new ResourceManager<FontResource, FontResourceDescriptor>();
            SoundResourceManager = new ResourceManager<SoundResource, SoundResourceDescriptor>();
            LevelManager = new LevelManager(this);
            PhysicsManager = new PhysicsManager(this);
            BlueprintManager = new BlueprintManager(this);
        }

        private void Initialize() {
            SceneManager.Initialize();
            EntityManager.Initialize();
            InputManager.Initialize();
            GUIManager.Initialize();
            AudioManager.Initialize();
            LevelManager.Initialize();
            PhysicsManager.Initialize();
        }

        private void _update(float deltaTime) {
            PhysicsManager.Update(deltaTime);
            SceneManager.Update(deltaTime);
            EntityManager.Update(deltaTime);
            Update(deltaTime);
        }

        private void _render(float ratioOfInterpolation) {
            SceneManager.Render(ratioOfInterpolation);
            GUIManager.Render();
            Render(ratioOfInterpolation);
        }

        private void UpdateTime(Stopwatch time, ref float oldTime, ref float newTime, ref float frameTime, ref float accumulator) {
            newTime = (float)time.Elapsed.TotalSeconds;
            frameTime = newTime - oldTime;
            oldTime = newTime;
            accumulator += frameTime;
        }

        public void Run() {
            Initialize();
            Load();

            Stopwatch time = Stopwatch.StartNew();

            const float deltaTime = 1.0f/30.0f;
            float oldTime = 0.0f;
            float newTime = 0.0f;
            float frameTime = 0.0f;
            float accumulator = 0.0f;

            MessagePump.Run(Form, () => {
                UpdateTime(time, ref oldTime, ref newTime, ref frameTime, ref accumulator);

                while (accumulator >= deltaTime) {
                    _update(deltaTime);
                    accumulator -= deltaTime;
                }

                if(GraphicsDevice.IsDeviceLost()) {
                    Thread.Sleep(100);
                }
                else {
                    GraphicsDevice.ClearBuffer(Color.Black);
                    Console.WriteLine((accumulator/deltaTime).ToString());
                    _render(accumulator / deltaTime);
                    GraphicsDevice.PresentBuffer();
                }
            });

            time.Stop();
            Unload();
        }

        public void Dispose() {
            SceneManager.Dispose();
            GraphicsDevice.Dispose();
            TextureResourceManager.Dispose();
            SoundResourceManager.Dispose();
            FontResourceManager.Dispose();
            GUIManager.Dispose();
        }

        public void Quit() {
            Settings.Save("Settings.xml");
            Form.Close(); 
        }

        public abstract void Load();
        public abstract void Update(float deltaTime);
        public abstract void Render(float ratioOfInterpolation);
        public abstract void Unload();

    }

}

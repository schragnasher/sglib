﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace SimpleGamesLibrary {

    public class Settings {

        public string GameName = "My Game";
        public float MasterVolume = 100;
        public float SoundVolume = 100;
        public float MusicVolume = 100;
        public int ResolutionX = 800;
        public int ResolutionY = 600;
        public bool Fullscreen = false;
        public bool VSync = true;

        //  Private constructor, use the static Load Method
        private Settings() { }

        public void Save(String filename) {
            XmlSerializer xs = new XmlSerializer(typeof(Settings));
            using (StreamWriter wr = new StreamWriter(filename)) {
                xs.Serialize(wr, this);
            } 
        }

        public static Settings Load(String filename) {
            if (!File.Exists(filename)) {
                Settings newSettings = new Settings();
                newSettings.Save(filename);
                return newSettings;
            }
            XmlSerializer xs = new XmlSerializer(typeof(Settings));
            using (StreamReader rd = new StreamReader(filename)) {
                return (Settings)xs.Deserialize(rd);
            } 
        }

    }

}
